`timescale 1ns / 1ps

/*
forward propagation of a three-layer* fully-connected neural network
*(excluding activation layers):

Sequential(
  (0): Linear(in_features=784, out_features=128, bias=True)
  (1): ReLU()
  (2): Linear(in_features=128, out_features=64, bias=True)
  (3): ReLU()
  (4): Linear(in_features=64, out_features=10, bias=True)
  (5): Softmax()
)

TODO    fprop: replace l0 assets with linear module
FIX     fprop: replace l2 assets with linear module
DONE    fprop: replace l1 assets with gen_relu module
DONE    fprop: replace l4 assets with linear module
 */

module fprop_ctrl_unit
(
    input                   clk,
    input                   start,
    input                   rst,
    // control signals
    output  wire    [10:0]  addr,
    // l0 linear
    output  wire            l0_start,
    output  wire            l0_rst,
    input   wire            l0_busy,
    input   wire            l0_done,
    // l2 linear
    output  wire            l2_start,
    output  wire            l2_rst,
    input   wire            l2_busy,
    input   wire            l2_done,
    // l4 linear
    output  wire            l4_start,
    output  wire            l4_rst,
    input   wire            l4_busy,
    input   wire            l4_done,
    // l5 softmax
    output  wire            l5_start,
    input                   l5_get_max_done,
    input                   l5_get_max_busy,
    input                   l5_PLF_done,
    // status signal
    output  wire            busy,
    output  wire            done
);
    /*  state machine as follows:
                                                    major_state_bits
                          IDLE                          'b0_000
                            ↳ LAYER0                    'b1_000
            addr == 'd785       ↳ LAYER2                'b1_010
            addr == 'd129            ↳ LAYER4           'b1_100
            addr == 'd065                ↳ DONE         'b1_111

        sequence on each layer
            minor_state_bits  'h0 'h1 'h2                 'h3
            clk_cnt             0   1   2   3   ... last    last+1    
            acc_rst             1   1   0   0   0   0       0
            acc_en              0   1   1   1   1   1       0
            addr                0   0   1   2   ... last    last+1
     */
    reg     [5:0]   cs,     ns;
    initial begin   cs = 0; ns = 0; end

    reg    [8:0]    controls;
    assign      {
                    // l0 linear
                    l0_start,
                    l0_rst,
                    // l2 linear
                    l2_start,
                    l2_rst,
                    // l4 linear
                    l4_start,
                    l4_rst,
                    busy,
                    l5_start,
                    done
                } = controls;
    always @ (posedge clk)
        cs = ns;
    
    always @ (posedge clk)
    begin
        casex(cs) //                         1 1 2 2 4 4   5
            'b0_000_xx: begin   controls = 'b0_0_0_0_0_0_0_0_0; addr = 0;         end
            // l0 linear
            'b1_000_00: begin   controls = 'b0_1_0_0_0_0_1_0_0; addr = 0;         end
            'b1_000_01: begin   controls = 'b1_0_0_0_0_0_1_0_0; addr = 0;         end
            'b1_000_10: begin   controls = 'b0_0_0_0_0_0_1_0_0; addr = 0;         end
            // l2 linear
            'b1_010_00: begin   controls = 'b0_0_0_1_0_0_1_0_0; addr = 0;         end
            'b1_010_01: begin   controls = 'b0_0_1_0_0_0_1_0_0; addr = 0;         end
            'b1_010_10: begin   controls = 'b0_0_0_0_0_0_1_0_0; addr = 0;         end
            // l4 linear
            'b1_100_00: begin   controls = 'b0_0_0_0_0_1_1_0_0; addr = 0;         end
            'b1_100_01: begin   controls = 'b0_0_0_0_1_0_1_0_0; addr = 0;         end
            'b1_100_10: begin   controls = 'b0_0_0_0_0_0_1_0_0; addr = 0;         end
            // l5 softmax
            'b1_101_00: begin   controls = 'b0_0_0_0_0_0_0_1_0; addr = 0;         end
            'b1_101_01: begin 
                            if  (l5_get_max_busy) 
                            begin
                                controls = 'b0_0_0_0_0_0_0_0_0; addr = 0;
                            end
                            else
                            begin
                                controls = 'b0_0_0_0_0_0_0_1_0; addr = 0;
                            end
                        end
            'b1_101_10: begin 
                            if  (l5_PLF_done) 
                            begin
                                controls = 'b0_0_0_0_0_0_0_0_1; addr = 0;
                            end
                            else
                            begin
                                controls = 'b0_0_0_0_0_0_0_0_0; addr = 0;
                            end
                        end
            'b1_111_00: begin   controls = 'b0_0_0_0_0_0_0_0_1; addr = 0;         end
        endcase
    end

    always @ (posedge clk)
    begin
        if (rst) begin cs = 0; ns = 0; end
        else
        begin
            casex(cs)
                'b0_000_xx: ns = start          ? 'b1_000_00 : 'b0_000_00;
                // l0 linear
                'b1_000_00: begin
                                if (l0_busy | l0_done) ns = 'b1_000_00;
                                else                   ns = 'b1_000_01;
                            end
                'b1_000_01: begin
                                if (!l0_busy)          ns = 'b1_000_01;
                                else                   ns = 'b1_000_10;
                            end
                'b1_000_10: begin
                                if (!l0_done)          ns = 'b1_000_10;
                                else                   ns = 'b1_010_00;
                            end
                // l2 linear
                'b1_010_00: begin
                                if (l2_busy | l2_done) ns = 'b1_010_00;
                                else                   ns = 'b1_010_01;
                            end
                'b1_010_01: begin
                                if (!l2_busy)          ns = 'b1_010_01;
                                else                   ns = 'b1_010_10;
                            end
                'b1_010_10: begin
                                if (!l2_done)          ns = 'b1_010_10;
                                else                   ns = 'b1_100_00;
                            end
                // l4 linear
                'b1_100_00: begin
                                if (l4_busy | l4_done) ns = 'b1_100_00;
                                else                   ns = 'b1_100_01;
                            end
                'b1_100_01: begin
                                if (!l4_busy)          ns = 'b1_100_01;
                                else                   ns = 'b1_100_10;
                            end
                'b1_100_10: begin
                                if (!l4_done)          ns = 'b1_100_10;
                                else                   ns = 'b1_101_00;
                            end
                // l5 softmax
                'b1_101_00: ns =                  'b1_101_01;
                'b1_101_01: begin
                                if  (l5_get_max_busy)
                                begin
                                    ns = 'b1_101_10;
                                end
                                else
                                begin
                                    ns = 'b1_101_01;
                                end
                            end
                'b1_101_10: begin
                                if  (l5_PLF_done)
                                begin
                                    ns = 'b1_111_00;
                                end
                                else
                                begin
                                    ns = 'b1_101_10;
                                end
                            end
                'b1_111_00: ns =                  'b1_111_00;
            endcase
        end
    end 
endmodule

module fprop
#(
    parameter DATA_WIDTH = 32
)
(
    input                   clk,
    input                   rst,
    // output
    output          [31:0]  result  [0:9],
    // status
    output                  busy,
    output                  done
);
    genvar i;       

    parameter                   d_in_channels = 784;
    reg signed [DATA_WIDTH-1:0] d_ins [0:d_in_channels-1];
    initial
    begin
        $readmemh("in1.dat", d_ins);
    end

    // l0 linear
    parameter       l0_in_channels  = 784;
    parameter       l0_out_channels = 128;
    wire            l0_start;
    wire            l0_rst;
    wire            l0_busy;
    wire            l0_done;

    wire    signed  [DATA_WIDTH-1:0]    l0_ins  [0:l0_in_channels];
    wire    signed  [DATA_WIDTH-1:0]    l0_outs [0:l0_out_channels-1];

    generate
        for (i = 0; i < l0_in_channels; i = i + 1)
        begin
            assign l0_ins[i] = d_ins[i];
        end
    endgenerate
    assign l0_ins[l0_in_channels] = 1'b1 << 26;

    linear #(
        .FN_PREFIX("layer0"),
        .IN_CHANNELS(l0_in_channels),
        .OUT_CHANNELS(l0_out_channels),
        .DATA_WIDTH(DATA_WIDTH)
    ) l0_linear (
        // data
        .d_ins(l0_ins),
        .d_outs(l0_outs),
        // control signals
        .clk(clk),
        .start(l0_start),
        .rst(l0_rst),
        // status signals
        .busy(l0_busy),
        .done(l0_done)
    );

    // l1 relu
    parameter       l1_channels       = l0_out_channels;
    wire    signed  [DATA_WIDTH-1:0]    l1_outs      [0:l1_channels-1];

    relu #(
        .CHANNELS(l1_channels)
    ) l1_relu (
        .d_ins(l0_outs),
        .d_outs(l1_outs)
    );

    // l2 linear
    parameter       l2_in_channels  = 128;
    parameter       l2_out_channels =  64;
    wire            l2_start;
    wire            l2_rst;
    wire            l2_busy;
    wire            l2_done;

    wire    signed  [DATA_WIDTH-1:0]    l2_ins  [0:l2_in_channels];
    wire    signed  [DATA_WIDTH-1:0]    l2_outs [0:l2_out_channels-1];

    generate
        for (i = 0; i < l2_in_channels; i = i + 1)
        begin
            assign l2_ins[i] = l1_outs[i];
        end
    endgenerate
    assign l2_ins[l2_in_channels] = 1'b1 << 26;

    linear #(
        .FN_PREFIX("layer2"),
        .IN_CHANNELS(l2_in_channels),
        .OUT_CHANNELS(l2_out_channels),
        .DATA_WIDTH(DATA_WIDTH)
    ) l2_linear (
        // data
        .d_ins(l2_ins),
        .d_outs(l2_outs),
        // control signals
        .clk(clk),
        .start(l2_start),
        .rst(l2_rst),
        // status signals
        .busy(l2_busy),
        .done(l2_done)
    );


    // l3 relu
    parameter       l3_channels       = l2_out_channels;
    wire    signed  [DATA_WIDTH-1:0]    l3_outs      [0:l3_channels-1];

    relu #(
        .CHANNELS(l3_channels)
    ) l3_relu (
        .d_ins(l2_outs),
        .d_outs(l3_outs)
    );


    // l4 linear
    parameter       l4_in_channels  = l3_channels;
    parameter       l4_out_channels = 10;
    wire            l4_start;
    wire            l4_rst;
    wire            l4_busy;
    wire            l4_done;
    
    wire    signed  [DATA_WIDTH-1:0]    l4_ins  [0:l4_in_channels];
    wire    signed  [DATA_WIDTH-1:0]    l4_outs [0:l4_out_channels-1];

    generate
        for (i = 0; i < l4_in_channels; i = i + 1)
        begin
            assign l4_ins[i] = l3_outs[i];
        end
    endgenerate
    assign l4_ins[l4_in_channels] = 1'b1 << 26;

    linear #(
        .FN_PREFIX("layer4"),
        .IN_CHANNELS(l4_in_channels),
        .OUT_CHANNELS(l4_out_channels),
        .DATA_WIDTH(DATA_WIDTH)
    ) l4_linear (
        // data
        .d_ins(l4_ins),
        .d_outs(l4_outs),
        // control signals
        .clk(clk),
        .start(l4_start),
        .rst(l4_rst),
        // status signals
        .busy(l4_busy),
        .done(l4_done)
    );


    // l5 softmax
    parameter               l5_channels = l4_out_channels;

    wire                    l5_start;
    wire                    l5_get_max_done;
    wire                    l5_get_max_busy;
    wire                    l5_PLF_done;

    wire    signed  [31:0]  l5_result     [0:l5_channels-1];
    softmax l5_softmax (
        .out_prev(l4_outs),
        .clk(clk),
        .start(l5_start),
        .out_result(l5_result),
        .get_max_done(l5_get_max_done),
        .get_max_busy(l5_get_max_busy),
        .PLF_done(l5_PLF_done)
    );

    generate
        for (i = 0; i < l5_channels; i = i + 1)
        begin
            assign result[i] = l5_result[i];
        end
    endgenerate


    fprop_ctrl_unit c0    (
        // inputs
        .clk(clk),
        .start(!rst),
        .rst(rst),
        // control signals
        // .addr(addr),
        // l0 linear
        .l0_start(l0_start),
        .l0_rst(l0_rst),
        .l0_busy(l0_busy),
        .l0_done(l0_done),
        // l2 linear
        .l2_start(l2_start),
        .l2_rst(l2_rst),
        .l2_busy(l2_busy),
        .l2_done(l2_done),
        // l4 linear
        .l4_start(l4_start),
        .l4_rst(l4_rst),
        .l4_busy(l4_busy),
        .l4_done(l4_done),
        // l5 softmax
        .l5_start(l5_start),
        .l5_get_max_done(l5_get_max_done),
        .l5_get_max_busy(l5_get_max_busy),
        .l5_PLF_done(l5_PLF_done),
        // status signal
        .busy(busy),
        .done(done)
    );

    // always@ (posedge clk)
    // begin
    //     if(!rst)
    //     begin
    //         addr = addr + 1;
    //     end
    // end

    // always@ (negedge rst)
    // begin
    //     addr = 0;
    // end
endmodule