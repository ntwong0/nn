
module relu_single
#(
    parameter DATA_WIDTH    = 32
)
(
    input   signed  [DATA_WIDTH-1:0]  d_in,
    output  signed  [DATA_WIDTH-1:0]  d_out
);
    assign d_out = d_in > 0 ? d_in : 0;
endmodule

module relu
#(
    parameter CHANNELS      = 10,
    parameter DATA_WIDTH    = 32
)
(
    input   signed  [DATA_WIDTH-1:0]  d_ins   [0:CHANNELS-1],
    output  signed  [DATA_WIDTH-1:0]  d_outs  [0:CHANNELS-1]
);
    genvar i;
    generate
        for (i = 0; i < CHANNELS; i = i + 1)
        begin
            relu_single relu_single
            (
                .d_in(d_ins[i]),
                .d_out(d_outs[i])
            );
        end
    endgenerate
endmodule