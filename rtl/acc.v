
module acc
(
    input                   clk,
    input                   en,
    input                   rst,
    input   signed  [31:0]  d_in,
    output  signed  [31:0]  d_out
);
    reg     signed  [31:0]  d_acc;
    initial                 d_acc = 'h0;

    integer i = 0;

    always@ (posedge clk)
    begin
        if(en)
        begin
            if(rst) d_acc = 'h0;
            else    d_acc = d_in + d_acc;
        end
        else
        begin
                    d_acc = d_acc;
        end
    end

    assign d_out = d_acc;
endmodule
