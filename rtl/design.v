`timescale 1ns / 1ps
// default values are tuned to MNIST handwritten digit dataset
/*
    input
    weights
    biases

    f1 = W1 * x  + b1
    f2 = W2 * relu(f1) + b2
 */

module relu
#(
    parameter DATUM_WIDTH = 16, // 8 bits per pixel * 2
    parameter DATA_COUNT = 785  // 28 x 28 images + 1 for bias
)
(
    input  wire [DATUM_WIDTH-1:0] x [0:DATA_COUNT-1],
    output reg  [DATUM_WIDTH-1:0] y [0:DATA_COUNT-1]
);
    integer i;
    always@ (*)
    begin
        for(i = 0; i < DATA_COUNT; i = i + 1)
        begin
            y[i] = x[i][DATUM_WIDTH-1] ==  1'b1 ?  0 : x[i]; 
        end
    end
endmodule

module vec_mat_mulacc
#(
    parameter DATUM_WIDTH = 8, // 8 bits per pixel
    parameter DATA_COUNT  = 784,
    parameter FEATURES    = 100
)
(
    input  wire [DATUM_WIDTH-1:0] x [0:DATA_COUNT-1], 
    input  wire [DATUM_WIDTH-1:0] w [0:DATA_COUNT-1][0:FEATURES-1],
    output reg  [DATUM_WIDTH-1:0] y [0:FEATURES-1]
);
    integer i,j;
    always@ (*)
    begin
        for(i = 0; i < FEATURES; i = i + 1)
        begin
            y[i] = 0;
            for(j = 0; j < DATA_COUNT; j = j + 1)
            begin
                y[i] = y[i] + x[j] * w[j][i];
            end
        end
    end
endmodule

// module in_var_mem
// (
//     // input
//     input  wire [8:0] data_in,
//     // control
//     input  wire       en,
//     // output
//     output wire [8:0] data_out
// );
//     reg         [8:0] mem      [0:63];
//     integer           n;

//     initial
//         for (n = 0; n<64; n=n+1)
//         begin
//             ram[n] = 8'FF;
//         end
// endmodule

module imem
(
    input   [ 5:0] a,
    output  [31:0] dOut);
);
    reg     [31:0] rom[0:63];
    initial
        $readmemh("memfile.dat", rom);
    assign dOut = rom[a];
endmodule