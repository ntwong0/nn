
module mem
#(
    parameter SIZE      = 128,
    parameter FILENAME  = "l3_out.dat",
    parameter CHANNELS  = 1,
    parameter OFFSET    = 0
)
(
    input   wire            [31:0] addr,
    output  wire    signed  [31:0] d_out
    // output  wire    signed  [31:0] d_out    [0:CHANNELS-1]
);
    reg     [31:0] rom[0:SIZE];

    initial
    begin
        $readmemh(FILENAME, rom);
        rom[SIZE] = 'd1 << 26;
    end

    // genvar i;
    // generate
    //     for (i = 0; i < CHANNELS; i = i + 1)
    //     begin
    //         assign d_out[i] = rom[addr + OFFSET * i];
    //     end
    // endgenerate
    assign d_out = rom[addr];
endmodule
