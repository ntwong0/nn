module pulsedemo_top (
	input CLOCK,
	output PULSE
);
//	assign PULSE = CLOCK;
	reg [10:0]	counter = 'd0;
	reg 		pulse_int = 'b0;
	reg 		pulse2_int = 'b0;

	always@ (posedge CLOCK)
	begin
		counter <= counter + 'd1;
		pulse_int <= 'b1;
		if (counter > 'd6)
		begin
			pulse_int <= 'b0;
		end
		if (counter > 'd800) 
		begin
			counter <= 'd0;
		end
	end

	always@ (posedge CLOCK)
	begin
		pulse2_int <= 'b0;
		if (counter > 500)
		begin
			pulse2_int <= 1'b1;
		end
	end
endmodule
