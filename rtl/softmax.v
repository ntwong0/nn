`timescale 1ns / 1ps

// TODO implement a proper reset

/* 
def softmax(x):
    e_x = np.exp(x - np.max(x))
    return e_x / e_x.sum(axis=0)

how does softmax work
    0. softmax acts on an array of values
    1. search through the array to find the max value
        (if we don't care about for inference we can stop here, we just need to 
        retrieve the index of the max value here)
    2. subtract the max value from the array elementwise 
        (this improves numerical stability and brings our values into the 
        negative regime, simplifying the approximation of step 3)
    3. exponentiate the array elementwise by Euler's number
    4. sum the array (this is the array sum for step 5)
    5. divide the array elementwise by the array sum
 */

/* 
    1. search through the array to find the max value
 */
module get_max
#(
    parameter               CHANNELS = 10
)
(   
    input   wire                    clk,
    input   wire                    start,
    input   wire    signed  [31:0]  out_prev    [0:CHANNELS-1],

    output  reg                     done,
    output  reg                     busy,

    output  wire    signed  [31:0]  out_max
);
    reg             [15:0]          max_index;
    reg             [15:0]          count;
    initial                         count = 0;

    always@ (posedge clk)
    begin
        if (!busy)
        begin
            if (start)
            begin
                done        <= 0;
                busy        <= 1;
                count       <= 0;
                max_index   <= 0;
            end
        end
        else // busy
        begin
            count           <= count + 1;
            if (out_prev[count] > out_prev[max_index])
            begin
                max_index   <= count;
            end
            if (count == CHANNELS - 1)
            begin
                done        <= 1;
                busy        <= 0;
                
            end
        end
    end

    assign out_max = out_prev[max_index];
endmodule

/* 
    2. subtract the max value from the array elementwise 
 */
module subtract_max_passthrough
#(
    parameter       CHANNELS = 10
)
(
    input   wire    signed  [31:0]  max_value,
    input   wire    signed  [31:0]  out_prev        [0:CHANNELS-1],
    output  wire    signed  [31:0]  out_stabilized  [0:CHANNELS-1]
);
    genvar i;
    generate
        for(i = 0; i < CHANNELS; i = i + 1)
        begin
            assign out_stabilized[i] = out_prev[i] - max_value;
        end
    endgenerate
endmodule

/* 
    3. exponentiate the array elementwise by Euler's number
 */
module PLF // piecewise linear function
#(
    parameter   CHANNELS = 10,
    parameter   LUT_SIZE = 64,
    
    parameter   FN_M = "softmax_m.dat",
    parameter   FN_B = "softmax_b.dat"
    // parameter   FN_X = "softmax_x.dat",    
)
(
    input   wire            clk,
    input   wire            start,

    input   wire    signed  [31:0]  in_x    [0:CHANNELS-1],

    output  reg             done,
    output  reg             busy,

    output  reg             [31:0] exp_x    [0:CHANNELS-1]
);
    reg     [52:0]  rom_m   [0:LUT_SIZE - 1];
    reg     [52:0]  rom_b   [0:LUT_SIZE - 1];
    // reg             [31:0]  rom_x   [0:LUT_SIZE - 1];

    // reg     [31:0]  exp_x   [0:CHANNELS-1];
    reg     [15:0]  count;


    wire    [84:0]  product;
    wire    [84:0]  sum;
    wire    [31:0]  sum_adjusted;
    wire    [15:0]  index;
    reg     [15:0]  addr_w;

    initial
    begin
        $readmemh(FN_M, rom_m);
        $readmemh(FN_B, rom_b);
        // $readmemh(FN_X, rom_x);
    end

    always@ (posedge clk)
    begin
        if (!busy)
        begin
            if (start)
            begin
                done        <= 0;
                busy        <= 1;
                count       <= 0;
            end
        end
        else // busy
        begin
            addr_w          <= count;
            count           <= count + 1;
            casex(in_x[count][31:25])
                'b1000_000: index = 63;  // 0x80000000,
                'b1000_001: index = 62;  // 0x82000000,
                'b1000_010: index = 61;  // 0x84000000,
                'b1000_011: index = 60;  // 0x86000000,
                'b1000_100: index = 59;  // 0x88000000,
                'b1000_101: index = 58;  // 0x8a000000,
                'b1000_110: index = 57;  // 0x8c000000,
                'b1000_111: index = 56;  // 0x8e000000,
                'b1001_000: index = 55;  // 0x90000000,
                'b1001_001: index = 54;  // 0x92000000,
                'b1001_010: index = 53;  // 0x94000000,
                'b1001_011: index = 52;  // 0x96000000,
                'b1001_100: index = 51;  // 0x98000000,
                'b1001_101: index = 50;  // 0x9a000000,
                'b1001_110: index = 49;  // 0x9c000000,
                'b1001_111: index = 48;  // 0x9e000000,
                'b1010_000: index = 47;  // 0xa0000000,
                'b1010_001: index = 46;  // 0xa2000000,
                'b1010_010: index = 45;  // 0xa4000000,
                'b1010_011: index = 44;  // 0xa6000000,
                'b1010_100: index = 43;  // 0xa8000000,
                'b1010_101: index = 42;  // 0xaa000000,
                'b1010_110: index = 41;  // 0xac000000,
                'b1010_111: index = 40;  // 0xae000000,
                'b1011_000: index = 39;  // 0xb0000000,
                'b1011_001: index = 38;  // 0xb2000000,
                'b1011_010: index = 37;  // 0xb4000000,
                'b1011_011: index = 36;  // 0xb6000000,
                'b1011_100: index = 35;  // 0xb8000000,
                'b1011_101: index = 34;  // 0xba000000,
                'b1011_110: index = 33;  // 0xbc000000,
                'b1011_111: index = 32;  // 0xbe000000,
                'b1100_000: index = 31;  // 0xc0000000,
                'b1100_001: index = 30;  // 0xc2000000,
                'b1100_010: index = 29;  // 0xc4000000,
                'b1100_011: index = 28;  // 0xc6000000,
                'b1100_100: index = 27;  // 0xc8000000,
                'b1100_101: index = 26;  // 0xca000000,
                'b1100_110: index = 25;  // 0xcc000000,
                'b1100_111: index = 24;  // 0xce000000,
                'b1101_000: index = 23;  // 0xd0000000,
                'b1101_001: index = 22;  // 0xd2000000,
                'b1101_010: index = 21;  // 0xd4000000,
                'b1101_011: index = 20;  // 0xd6000000,
                'b1101_100: index = 19;  // 0xd8000000,
                'b1101_101: index = 18;  // 0xda000000,
                'b1101_110: index = 17;  // 0xdc000000,
                'b1101_111: index = 16;  // 0xde000000,
                'b1110_000: index = 15;  // 0xe0000000,
                'b1110_001: index = 14;  // 0xe2000000,
                'b1110_010: index = 13;  // 0xe4000000,
                'b1110_011: index = 12;  // 0xe6000000,
                'b1110_100: index = 11;  // 0xe8000000,
                'b1110_101: index = 10;  // 0xea000000,
                'b1110_110: index =  9;  // 0xec000000,
                'b1110_111: index =  8;  // 0xee000000,
                'b1111_000: index =  7;  // 0xf0000000,
                'b1111_001: index =  6;  // 0xf2000000,
                'b1111_010: index =  5;  // 0xf4000000,
                'b1111_011: index =  4;  // 0xf6000000,
                'b1111_100: index =  3;  // 0xf8000000,
                'b1111_101: index =  2;  // 0xfa000000,
                'b1111_110: index =  1;  // 0xfc000000,
                'b1111_111: index =  0;  // 0xfe000000,
                default   : index =  0;
            endcase
            // exp_x[count]     = sum[52:21];
            exp_x[addr_w]     = sum_adjusted;
            if (count == CHANNELS)
            begin
                done        <= 1;
                busy        <= 0;
            end
        end
    end

    /* 
        product is 85-bits, since rom_m is 53 bits and in_x is 32 bits
        we need to shift product to the left by 6 bits to align the 
        radix point to match rom_b. in addition, we need to resize
        product to 53 bits to match rom_b by discarding lsbs

        since x is negative, we can reduce complexity by taking its complement 
        m*x can then proceed unsigned, and the sign is accounted for via 
        b - m*x
    */  

    wire    [31:0]  in_x_complement;
    assign          in_x_complement = ~in_x[addr_w] + 1; // twos-complement
    assign          product  = rom_m[index] * in_x_complement;
    assign          sum = (offset << 26) - product;
    assign          sum_adjusted = sum >> 47;

    wire   [52:0]   slope;
    assign          slope    = rom_m[index];
    wire   [52:0]   offset;
    assign          offset   = rom_b[index];
    wire   [84:0]   offset_adjusted;
    assign          offset_adjusted   = rom_b[index] << 26;

endmodule

/*
    4. sum the array (this is the array sum for step 5)
 */
module sum
#(
    parameter       CHANNELS=10
)
(
    input   [31:0]  in_x    [0:CHANNELS-1],
    output  [31:0]  out_sum
);
    wire    [31:0]  i_sum   [0:CHANNELS-2]; // intermediate sum
    assign i_sum[0] = in_x[0];

    genvar i;
    generate
        for (i = 1; i < CHANNELS-1; i = i + 1)
        begin
            assign i_sum[i] = in_x[i] + i_sum[i-1];
        end
    endgenerate

    assign out_sum  = in_x[CHANNELS-1] + i_sum[CHANNELS-2];
endmodule

/* 
    5. divide the array elementwise by the array sum
 */
module divide_elementwise
#(  
    parameter       CHANNELS=10
)
(
    input   [63:0]  in_x    [0:CHANNELS-1],
    input   [31:0]  in_sum,
    output  [31:0]  out_x   [0:CHANNELS-1]
);
    genvar i;
    generate
        for(i = 0; i < CHANNELS; i = i + 1)
        begin
            assign out_x[i] = in_x[i] / in_sum;
        end
    endgenerate
endmodule

module softmax(
    // data input
    input   [31:0]  out_prev        [0:9],
    // control input
    input           clk,
    input           start,
    // data output
    output  [31:0]  out_result      [0:9],
    // status output
    output          get_max_done,
    output          get_max_busy, 
    output          PLF_done
);
    reg     [31:0]  out_prev        [0:9];  // input
    wire    [31:0]  out_stabilized  [0:9];
    wire    [31:0]  out_exp         [0:9];
    wire    [31:0]  max_value;
    wire    [31:0]  out_sum;
    wire    [31:0]  out_result      [0:9];  // output
    
    reg             PLF_wait;
    reg             PLF_start;
    reg             PLF_busy;
    reg             PLF_done;
    initial
    begin
        PLF_wait    = 0;
        PLF_start   = 0;
        PLF_busy    = 0;
        PLF_done    = 0;
    end

    // controller
    always@ (posedge clk)
    begin
        if(get_max_busy)
        begin
            PLF_wait = 1;
        end
        if(get_max_done && PLF_wait)
        begin
            PLF_wait  = 0;
            PLF_start = 1;
        end
        if(PLF_start && PLF_busy)
        begin
            PLF_start = 0;
        end
    end

    get_max get_max
    (
        .clk(clk),
        .start(start),
        .out_prev(out_prev),
        .done(get_max_done),
        .busy(get_max_busy),
        .out_max(max_value)
    );

    subtract_max_passthrough subtract_max_passthrough
    (
        .max_value(max_value),
        .out_prev(out_prev),
        .out_stabilized(out_stabilized)
    );

    PLF PLF
    (
        .clk(clk),
        .start(PLF_start),
        .in_x(out_stabilized),
        .done(PLF_done),
        .busy(PLF_busy),
        .exp_x(out_exp)
    );

    sum sum 
    (
        .in_x(out_exp),
        .out_sum(out_sum)
    );

    wire    [63:0]  out_exp_64  [0:9];
    genvar i;
    generate
        for(i = 0; i < 10; i = i + 1)
        begin
            assign out_exp_64[i] = {6'b0, out_exp[i], 26'b0};
        end
    endgenerate 

    divide_elementwise divide_elementwise
    (
        .in_x  (out_exp_64),
        .in_sum(out_sum),
        .out_x (out_result)
    );

endmodule


module tb(
    input  clk,
    input  start,
    output get_max_done,
    output get_max_busy, 
    output PLF_done
);
    reg     [31:0]  out_prev        [0:9];
    wire    [31:0]  out_result      [0:9];

    initial
    begin
        // out_prev[0] = 'h00200000;
        // out_prev[1] = 'h00400000;
        // out_prev[2] = 'h00800000;
        // out_prev[3] = 'h01000000;
        // out_prev[4] = 'h02000000;
        // out_prev[5] = 'h04000000;
        // out_prev[6] = 'h08000000;
        // out_prev[7] = 'h10000000;
        // out_prev[8] = 'h20000000;
        // out_prev[9] = 'h40000000;

        out_prev[0] = 'he63fb5c0;
        out_prev[1] = 'h265dba40;
        out_prev[2] = 'hf90155d0;
        out_prev[3] = 'h057a1088;
        out_prev[4] = 'h0058feee;
        out_prev[5] = 'hf5fa68a0;
        out_prev[6] = 'hf0bcad50;
        out_prev[7] = 'h0be4bdb0;
        out_prev[8] = 'hfa2c0ed8;
        out_prev[9] = 'h0527a628;
    end

    softmax softmax (
        .out_prev(out_prev),
        .clk(clk),
        .start(start),
        .out_result(out_result),
        .get_max_done(get_max_done),
        .get_max_busy(get_max_busy),
        .PLF_done(PLF_done)
    );

endmodule