`timescale 1ns / 1ps

module ctrl_unit
(
    input                   clk,
    input                   start,
    input                   rst,
    // control signals
    output  wire    [10:0]  addr,
    output  wire            l0_acc_en,
    output  wire            l0_acc_rst,
    output  wire            l2_acc_en,
    output  wire            l2_acc_rst,
    output  wire            l4_acc_en,
    output  wire            l4_acc_rst,
    // status signal
    output  wire            busy,
    output  wire            done
);
    /*  state machine as follows:
                                                    major_state_bits
                          IDLE                          'b0_000
                            ↳ LAYER0                    'b1_000
            addr == 'd785       ↳ LAYER2                'b1_010
            addr == 'd129            ↳ LAYER4           'b1_100
            addr == 'd065                ↳ DONE         'b1_111

        sequence on each layer
            minor_state_bits  'h0 'h1 'h2                 'h3
            clk_cnt             0   1   2   3   ... last    last+1    
            acc_rst             1   1   0   0   0   0       0
            acc_en              0   1   1   1   1   1       0
            addr                0   0   1   2   ... last    last+1
     */
    reg     [5:0]   cs,     ns;
    initial begin   cs = 0; ns = 0; end

    reg    [7:0]    controls;
    assign      {
                    l0_acc_rst,
                    l0_acc_en,
                    l2_acc_rst,
                    l2_acc_en,
                    l4_acc_rst,
                    l4_acc_en,
                    busy,
                    done
                } = controls;
    always @ (posedge clk)
        cs = ns;
    
    always @ (posedge clk)
    begin
        casex(cs)
            'b0_000_xx: begin controls = 'b0_0_0_0_0_0_0_0; addr = 0;           end
            'b1_000_00: begin controls = 'b1_0_0_0_0_0_1_0; addr = 0;           end
            'b1_000_01: begin controls = 'b1_1_0_0_0_0_1_0; addr = 0;           end        
            'b1_000_10: begin controls = 'b0_1_0_0_0_0_1_0; addr = addr + 1;    end        
            'b1_000_11: begin controls = 'b0_0_0_0_0_0_1_0; addr = 0;           end
            'b1_010_00: begin controls = 'b0_0_1_0_0_0_1_0; addr = 0;           end
            'b1_010_01: begin controls = 'b0_0_1_1_0_0_1_0; addr = 0;           end        
            'b1_010_10: begin controls = 'b0_0_0_1_0_0_1_0; addr = addr + 1;    end        
            'b1_010_11: begin controls = 'b0_0_0_0_0_0_1_0; addr = 0;           end
            'b1_100_00: begin controls = 'b0_0_0_0_1_0_1_0; addr = 0;           end
            'b1_100_01: begin controls = 'b0_0_0_0_1_1_1_0; addr = 0;           end        
            'b1_100_10: begin controls = 'b0_0_0_0_0_1_1_0; addr = addr + 1;    end        
            'b1_100_11: begin controls = 'b0_0_0_0_0_0_1_0; addr = 0;           end
            'b1_111_00: begin controls = 'b0_0_0_0_0_0_0_1; addr = 0;           end
        endcase
    end

    always @ (posedge clk)
    begin
        if (rst) begin cs = 0; ns = 0; end
        else
        begin
            casex(cs)
                'b0_000_xx: ns = start          ? 'b1_000_00 : 'b0_000_00;
                'b1_000_00: ns =                  'b1_000_01;
                'b1_000_01: ns =                  'b1_000_10;
                'b1_000_10: ns = (addr < 'd785) ? 'b1_000_10 : 'b1_000_11;
                'b1_000_11: ns =                  'b1_010_00;
                'b1_010_00: ns =                  'b1_010_01;
                'b1_010_01: ns =                  'b1_010_10;
                'b1_010_10: ns = (addr < 'd129) ? 'b1_010_10 : 'b1_010_11;
                'b1_010_11: ns =                  'b1_100_00;
                'b1_100_00: ns =                  'b1_100_01;
                'b1_100_01: ns =                  'b1_100_10;
                'b1_100_10: ns = (addr < 'd065) ? 'b1_100_10 : 'b1_100_11;
                'b1_100_11: ns =                  'b1_111_00;
                'b1_111_00: ns =                  'b1_111_00;
            endcase
        end
    end 
endmodule

module mem
#(
    parameter SIZE      = 128,
    parameter FILENAME  = "l3_out.dat",
    parameter CHANNELS  = 1,
    parameter OFFSET    = 0
)
(
    input   wire            [31:0] addr,
    output  wire    signed  [31:0] d_out
    // output  wire    signed  [31:0] d_out    [0:CHANNELS-1]
);
    reg     [31:0] rom[0:SIZE];

    initial
    begin
        $readmemh(FILENAME, rom);
        rom[SIZE] = 'd1 << 26;
    end

    // genvar i;
    // generate
    //     for (i = 0; i < CHANNELS; i = i + 1)
    //     begin
    //         assign d_out[i] = rom[addr + OFFSET * i];
    //     end
    // endgenerate
    assign d_out = rom[addr];
endmodule

module acc
(
    input                   clk,
    input                   en,
    input                   rst,
    input   signed  [31:0]  d_in,
    output  signed  [31:0]  d_out
);
    reg     signed  [31:0]  d_acc;
    initial                 d_acc = 'h0;

    integer i = 0;

    always@ (posedge clk)
    begin
        if(en)
        begin
            if(rst) d_acc = 'h0;
            else    d_acc = d_in + d_acc;
        end
        else
        begin
                    d_acc = d_acc;
        end
    end

    assign d_out = d_acc;
endmodule

module relu
(
    input   signed  [31:0]  d_in,
    output  signed  [31:0]  d_out
);
    assign d_out = d_in > 0 ? d_in : 0;
endmodule

module dev_top
(
    input                   clk,
    input                   rst,
    // output
    output          [31:0]  result  [0:9],
    // status
    output                  busy,
    output                  done
);
    genvar          i;       

    wire    [31:0]  addr;

    wire signed [31:0] a, b, d;
    wire signed [63:0] c;
    initial 
    begin
        a = 'hfc000000; b = 'hffdbed62;
    end
    assign c = a * b;
    assign d = c[57:26];

    parameter       l0_channels = 128;
    wire            l0_acc_en;
    wire            l0_acc_rst;
    wire    signed  [31:0]  curr_l0_in;
    wire    signed  [31:0]  curr_l0_param [0:l0_channels-1];
    wire    signed  [63:0]  l0_product    [0:l0_channels-1];
    wire    signed  [31:0]  l0_result     [0:l0_channels-1];
    wire    signed  [31:0]  l1_out        [0:l0_channels-1];

    generate        
        for (i = 0; i < l0_channels ; i = i + 1)
        begin
            assign l0_product[i] = curr_l0_in * curr_l0_param[i];
        end
    endgenerate

    mem     #( 784,   "in1.dat")   l0_in  (
                                                .addr(addr),      
                                                .d_out(curr_l0_in)
                                            );
    
    // mem     #(  100480, 
    //             "layer2.dat", 
    //             10, 
    //             'd785)               l0_param    (
    //                                             .addr(addr), 
    //                                             .d_out(curr_l0_param)
    //                                         );
    generate
        for (i = 0; i < l0_channels; i = i + 1)
        begin
            mem #(100480, "layer0.dat") l0_param  (
                                                    .addr(addr+ 'd785 * i), 
                                                    .d_out(curr_l0_param[i])
                                                );
        end
    endgenerate

    generate
        for (i = 0; i < l0_channels; i = i + 1)
        begin
            acc l0_acc  (
                            .clk(clk),
                            .en(l0_acc_en),
                            .rst(l0_acc_rst),
                            .d_in(l0_product[i][57:26]),
                            .d_out(l0_result[i])
                        );
        end
    endgenerate

    generate
        for (i = 0; i < l0_channels; i = i + 1)
        begin
            relu l1_relu    (
                                .d_in(l0_result[i]),
                                .d_out(l1_out[i])
                            );
        end
    endgenerate
    // __TODO we only ever need one relu



    parameter       l2_channels = 64;
    wire            l2_acc_en;
    wire            l2_acc_rst;
    wire    signed  [31:0]  curr_l1_out;
    wire    signed  [31:0]  curr_l2_param [0:l2_channels-1];
    wire    signed  [63:0]  l2_product    [0:l2_channels-1];
    wire    signed  [31:0]  l2_result     [0:l2_channels-1];
    wire    signed  [31:0]  l3_out        [0:l2_channels-1];

    generate        
        for (i = 0; i < l2_channels ; i = i + 1)
        begin
            assign l2_product[i] = curr_l1_out * curr_l2_param[i];
        end
    endgenerate

    // mem     #( 128,   "out1.dat")   l1_out  (
    //                                             .addr(addr),      
    //                                             .d_out(curr_l1_out)
    //                                         );
    assign curr_l1_out = l1_out[addr];
    
    // mem     #(  8256, 
    //             "layer2.dat", 
    //             10, 
    //             'd129)               l2_param    (
    //                                             .addr(addr), 
    //                                             .d_out(curr_l2_param)
    //                                         );
    generate
        for (i = 0; i < l2_channels; i = i + 1)
        begin
            mem #(8256, "layer2.dat") l2_param  (
                                                    .addr(addr+ 'd129 * i), 
                                                    .d_out(curr_l2_param[i])
                                                );
        end
    endgenerate

    generate
        for (i = 0; i < l2_channels; i = i + 1)
        begin
            acc l2_acc  (
                            .clk(clk),
                            .en(l2_acc_en),
                            .rst(l2_acc_rst),
                            .d_in(l2_product[i][57:26]),
                            .d_out(l2_result[i])
                        );
        end
    endgenerate

    generate
        for (i = 0; i < l2_channels; i = i + 1)
        begin
            relu l3_relu    (
                                .d_in(l2_result[i]),
                                .d_out(l3_out[i])
                            );
        end
    endgenerate



    parameter       l4_channels = 10;
    wire            l4_acc_en;
    wire            l4_acc_rst;
    wire    signed  [31:0]  curr_l3_out;
    wire    signed  [31:0]  curr_l4_param [0:l4_channels-1];
    wire    signed  [31:0]  l4_result     [0:l4_channels-1];
    wire    signed  [63:0]  l4_product    [0:l4_channels-1];
    // wire    signed  [31:0]  
 
    generate        
        for (i = 0; i < l4_channels ; i = i + 1)
        begin
            assign l4_product[i] = curr_l3_out * curr_l4_param[i];
        end
    endgenerate

    // mem     #(  64,   "out3.dat")   l3_out  (
    //                                             .addr(addr),      
    //                                             .d_out(curr_l3_out)
    //                                         );
    assign curr_l3_out = l3_out[addr];    
    
    // mem     #(  650, 
    //             "layer4.dat", 
    //             10, 
    //             'd65)               l4_param    (
    //                                             .addr(addr), 
    //                                             .d_out(curr_l4_param)
    //                                         );
    generate
        for (i = 0; i < l4_channels; i = i + 1)
        begin
            mem #(650, "layer4.dat") l4_param   (
                                                    .addr(addr+ 'd65 * i), 
                                                    .d_out(curr_l4_param[i])
                                                );
        end
    endgenerate
    
    generate
        for (i = 0; i < l4_channels; i = i + 1)
        begin
            acc l4_acc  (
                            .clk(clk),
                            .en(l4_acc_en),
                            .rst(l4_acc_rst),
                            .d_in(l4_product[i][57:26]),
                            .d_out(l4_result[i])
                        );
        end
    endgenerate

    generate
        for (i = 0; i < l4_channels; i = i + 1)
        begin
            assign result[i] = l4_result[i];
        end
    endgenerate

    ctrl_unit c0    (
                        // inputs
                        .clk(clk),
                        .start(!rst),
                        .rst(rst),
                        // control signals
                        .addr(addr),
                        .l0_acc_en(l0_acc_en),
                        .l0_acc_rst(l0_acc_rst),
                        .l2_acc_en(l2_acc_en),
                        .l2_acc_rst(l2_acc_rst),
                        .l4_acc_en(l4_acc_en),
                        .l4_acc_rst(l4_acc_rst),
                        // status signal
                        .busy(busy),
                        .done(done)
                    );

    // always@ (posedge clk)
    // begin
    //     if(!rst)
    //     begin
    //         addr = addr + 1;
    //     end
    // end

    // always@ (negedge rst)
    // begin
    //     addr = 0;
    // end
endmodule