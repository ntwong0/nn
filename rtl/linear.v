/*

Note: As of 2020-09-14, Verilator is unable to fully support $readmemh(). In
    particular, IEEE Std 1800-2012 §21.4 specifies start_addr and end_addr as
    optional arguments, but these overloadings for $readmemh() are not 
    implemented.

    To overcome this, the layer weights are split into multiple files, and 
    loading of the data files are handled in the following way.

TODO parameterize products vector slicing within linear_acc generate block
TODO accommodate 3rd digit in linear_weight
DONE tb to test linear
DONE implement generate blocks for linear_weight module instances
DONE use INDEX to set FILENAME via incrementing + string concat
    ref1: https://stackoverflow.com/questions/17336636
    ref2: https://stackoverflow.com/questions/12379750
 */

module linear_weight // weights and biases for linear layers
// module linear // weights and biases for linear layers
#(
    parameter IN_CHANNELS   = 64,
    parameter OUT_CHANNELS  = 10,
    // parameter FILENAME      = "layer4.dat",
    parameter FN_PREFIX     = "layer4",
    parameter INDEX         = 1,
    parameter DATA_WIDTH    = 32
)
(
    input   wire            [DATA_WIDTH-1:0] addr,
    output  wire    signed  [DATA_WIDTH-1:0] w_out
);
    parameter   FILENAME    = {
        FN_PREFIX,
        // TEST accommodate 3rd digit
        // "-000" + (256 * (INDEX / 10)) + (INDEX % 10),
        "-000" + (65536 * (INDEX / 100)) + (256 * ((INDEX % 100) / 10)) + (INDEX % 10),
        ".dat"
    };
    parameter   SIZE        = IN_CHANNELS*OUT_CHANNELS + OUT_CHANNELS;
    parameter   START_INDEX = (IN_CHANNELS + 1) * INDEX;
    parameter   END_INDEX   = (IN_CHANNELS + 1) * (INDEX + 1) - 1;

    reg         [DATA_WIDTH-1:0] rom    [START_INDEX:END_INDEX];

    initial
    begin
        // $readmemh(FILENAME, rom, START_INDEX, END_INDEX);
        $readmemh(FILENAME, rom);
    end
    assign w_out = rom[addr];
endmodule

// module linear_weights
// #(
//     parameter IN_CHANNELS   = 64,
//     parameter OUT_CHANNELS  = 10,
//     // parameter FILENAME      = "layer4.dat",
//     parameter FN_PREFIX     = "layer4",
//     parameter INDEX         = 1,
//     parameter DATA_WIDTH    = 32
// )
// (
//     input   wire            [DATA_WIDTH-1:0]    addr,
//     output  wire    signed  [DATA_WIDTH-1:0]    w_outs  [0:OUT_CHANNELS-1]
// );
//     genvar i;
//     generate
//         for (i = 0; i < OUT_CHANNELS; i = i + 1)
//         begin
//             linear_weight #(
//                 // IN_CHANNELS
//                 // OUT_CHANNELS
//                 // FN_PREFIX
//                 .INDEX(i)
//                 // DATA_WIDTH
//             ) w_r (
//                 .addr(addr + (IN_CHANNELS + 1) * i),
//                 .w_out(w_outs[i])
//             );
//         end
//     endgenerate
// endmodule

module linear_acc
#(
    parameter DATA_WIDTH    = 32
)
(
    // data
    input   signed  [DATA_WIDTH-1:0]    d_in,
    output  signed  [DATA_WIDTH-1:0]    d_out,
    // control signals
    input                               clk,
    input                               en,
    input                               rst
);
    reg     signed  [DATA_WIDTH-1:0]    d_acc;
    initial d_acc = 'h0;

    integer i = 0;

    always@ (posedge clk)
    begin
        if(en)
        begin
            if(rst) d_acc = 'h0;
            else    d_acc = d_in + d_acc;
        end
        else
        begin
                    d_acc = d_acc;
        end
    end

    assign d_out = d_acc;
endmodule

// wire    signed  [(DATA_WIDTH << 1) - 1: 0]  products [0:OUT_CHANNELS-1];
// generate
//     for (i = 0; i < OUT_CHANNELS; i = i + 1)
//     begin
//         assign products[i] = w_outs[i] * d_ins[i];
//     end
// endgenerate
module mul
#(
    parameter   DATA_WIDTH      = 32,
    parameter   OUT_CHANNELS    = 10
)
(
    input   signed  [ DATA_WIDTH      - 1: 0] w_in,
    input   signed  [ DATA_WIDTH      - 1: 0] d_in,
    output  signed  [(DATA_WIDTH << 1)- 1: 0] product
);
    assign product = w_in * d_in;
endmodule

// module linear_dut
module linear
#(

    parameter FN_PREFIX     = "layer4",
    parameter IN_CHANNELS   = 64,
    parameter OUT_CHANNELS  = 10,
    parameter DATA_WIDTH    = 32
)
(
    // data
    input   signed [DATA_WIDTH-1:0]     d_ins   [0:IN_CHANNELS],
    output  signed [DATA_WIDTH-1:0]     d_outs  [0:OUT_CHANNELS-1],
    // control signals
    input   clk,
    input   start,
    input   rst,
    // status signals
    output  busy,
    output  done
);

    reg             [DATA_WIDTH-1:0]    addr;
    initial addr    = 0;
    wire    signed  [DATA_WIDTH-1:0]    weights  [0:OUT_CHANNELS-1];

    // weights and biases, by row
    // splitting of weight/bias params supported by splitter.py
    genvar i;
    generate
        for (i = 0; i < OUT_CHANNELS; i = i + 1)
        begin
            linear_weight #(
                .IN_CHANNELS(IN_CHANNELS),
                .OUT_CHANNELS(OUT_CHANNELS),
                .FN_PREFIX(FN_PREFIX),
                .INDEX(i),
                .DATA_WIDTH(DATA_WIDTH)
            ) w_r (
                .addr(addr + (IN_CHANNELS + 1) * i),
                .w_out(weights[i])
            );
        end
    endgenerate

    // multiply by row, producing ith term of partial sums
    wire    signed  [(DATA_WIDTH << 1) - 1: 0]  products [0:OUT_CHANNELS-1];
    generate
        for (i = 0; i < OUT_CHANNELS; i = i + 1)
        begin
            mul mul (
                .w_in(weights[i]),
                .d_in(d_ins[addr]),
                .product(products[i])
            );
        end
    endgenerate
    
    // accumulate partial sums per row
    reg     acc_en, acc_rst;
    initial 
    begin
        acc_en  = 0;
        acc_rst = 0;
    end
    generate
        for (i = 0; i < OUT_CHANNELS; i = i + 1)
        begin
            linear_acc acc (
                .clk(clk),
                .en(acc_en),
                .rst(acc_rst),
                // .d_in(products[i][57:26]),  // TODO parameterize this
                .d_in(products[i] >>> 26), 
                .d_out(d_outs[i])
            );
        end
    endgenerate

    // linear_ctrl
    reg     busy, done;
    initial
    begin
        busy = 0;
        done = 0;
    end
    
    always@ (posedge clk)
    begin
        if (rst)
        begin
            acc_en  = 0;
            acc_rst = 1;
            busy    = 0;
            done    = 0;
            addr    = 0;
        end
        else if (start & !busy & !done)
        begin
            acc_en  = 0;
            acc_rst = 0;
            busy    = 1;
            done    = 0;
            addr    = 0;
        end
        else if (busy & addr < IN_CHANNELS)
        begin
            acc_en  = 1;
            acc_rst = 0;
            busy    = 1;
            done    = 0;
            addr    = addr + 1;
        end
        else if (busy & addr >= IN_CHANNELS)
        begin
            acc_en  = 1;
            acc_rst = 0;
            busy    = 0;
            done    = 1;
            addr    = 0;
        end
        else if (done)
        begin
            acc_en  = 0;
            acc_rst = 0;
            busy    = 0;
            done    = 1;
            addr    = 0;
        end
    end
endmodule

module linear_tb 
// module linear
#(
    parameter IN_CHANNELS   = 64,
    parameter OUT_CHANNELS  = 10,
    parameter DATA_WIDTH    = 32
)
(
    output  signed [DATA_WIDTH-1:0]     d_outs  [0:OUT_CHANNELS-1],
    // control signals
    input                               clk,
    input                               start,
    input                               rst,
    // status signals
    output                              busy,
    output                              done
);
    // data
    reg     signed [DATA_WIDTH-1:0]     d_ins   [0:IN_CHANNELS];

    initial
    begin
        $readmemh("out3.dat", d_ins);
        d_ins[IN_CHANNELS] = 'd1 << 26;
    end

    // linear_dut linear_dut (
    linear linear (
        // data
        .d_ins(d_ins),
        .d_outs(d_outs),
        // control signals
        .clk(clk),
        .start(start),
        .rst(rst),
        // status signals
        .busy(busy),
        .done(done)
    );
endmodule