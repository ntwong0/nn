#!/usr/bin/env python

import argparse
import os

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="splits layer parameter dat files based on channel sizes"
    )
    parser.add_argument(
        "dat_file", 
        help="the file to be split"
    )
    parser.add_argument(
        "in_channels", 
        type=int,
        help="number of input channels for the layer"
    )
    parser.add_argument(
        "out_channels", 
        type=int,
        help="number of output channels for the layer"
    )
    args = parser.parse_args()

    base_fn = args.dat_file
    pref_fn = None
    if os.path.exists(base_fn):
        pref_fn = (os.path.splitext(args.dat_file)[0])
    else:
        print("DATFILE DOES NOT EXIST")
        exit()
    
    num_neurons = args.out_channels
    num_params = args.in_channels + 1 # number of weights + bias per neuron

    base_file = open(args.dat_file, 'r')
    with base_file as reader:
        for i in range(num_neurons): 
            curr_file = open(pref_fn + '-' + str(i).zfill(3) + '.dat', 'w')
            with curr_file as writer:
                for j in range(num_params): 
                    line = reader.readline()
                    writer.write(line)
                writer.close()
        reader.close()

