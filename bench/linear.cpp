#include <verilated.h>
#include "Vlinear.h"

#include "verilated_vcd_c.h"

#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>

Vlinear *uut;
vluint64_t main_time = 0; // ns

double sc_time_stamp () {
	return main_time;
}

int main(int argc, char** argv)
{
	bool vcdTrace = true;
	VerilatedVcdC* tfp = NULL;

	Verilated::commandArgs(argc, argv);
	uut = new Vlinear;

	uut->eval();
	uut->eval();

	if (vcdTrace)
	{
		Verilated::traceEverOn(true);

		tfp = new VerilatedVcdC;
		uut->trace(tfp, 99);

		std::string vcdname = argv[0];
		vcdname += ".vcd";
		std::cout << vcdname << std::endl;
		tfp->open(vcdname.c_str());
	}

	// uut->addr = 0;
    uut->clk    = 0;
    uut->start  = 0;
    uut->rst    = 0;

	uut->eval();

    bool overtime = false;
	while (!Verilated::gotFinish() && !overtime)
	{
		if ((main_time % 1 == 0) && (main_time > 0))
        {
            // uut->addr++;
            uut->clk = ~uut->clk;
        }

        if (main_time > 10 && !(uut->busy) && !(uut->done))
        {
            uut->start = 1;
        }
        else
        {
            uut->start = 0;
        }
        

		uut->eval();

		if (tfp != NULL)
		{
			tfp->dump (main_time);
		}

        if (main_time % 1000 == 0)
        {
            printf("main_time: %i\n", main_time);
        }

		main_time++;
        overtime = (main_time > 1000) ? true : false; // overall compute time
        // overtime = (main_time > 15800) ? true : false; // l0 compute time
        // overtime = (main_time > 2600) ? true : false; // l2+l3 compute time
        // overtime = (main_time > 1300) ? true : false; // l4 compute time
	}

	uut->final();

	if (tfp != NULL)
	{
		tfp->close();
		delete tfp;
	}

	delete uut;

	return 0;
}

