// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Design implementation internals
// See Vlinear.h for the primary calling header

#include "Vlinear.h"           // For This
#include "Vlinear__Syms.h"

//--------------------
// STATIC VARIABLES


//--------------------

VL_CTOR_IMP(Vlinear) {
    Vlinear__Syms* __restrict vlSymsp = __VlSymsp = new Vlinear__Syms(this, name());
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Reset internal values
    
    // Reset structure values
    { int __Vi0=0; for (; __Vi0<10; ++__Vi0) {
	    d_outs[__Vi0] = VL_RAND_RESET_I(32);
    }}
    clk = VL_RAND_RESET_I(1);
    start = VL_RAND_RESET_I(1);
    rst = VL_RAND_RESET_I(1);
    busy = VL_RAND_RESET_I(1);
    done = VL_RAND_RESET_I(1);
    { int __Vi0=0; for (; __Vi0<10; ++__Vi0) {
	    v__DOT__d_outs[__Vi0] = VL_RAND_RESET_I(32);
    }}
    { int __Vi0=0; for (; __Vi0<65; ++__Vi0) {
	    v__DOT__d_outs_prev[__Vi0] = VL_RAND_RESET_I(32);
    }}
    { int __Vi0=0; for (; __Vi0<10; ++__Vi0) {
	    v__DOT__linear_dut__DOT__d_outs[__Vi0] = VL_RAND_RESET_I(32);
    }}
    v__DOT__linear_dut__DOT__addr = VL_RAND_RESET_I(32);
    { int __Vi0=0; for (; __Vi0<10; ++__Vi0) {
	    v__DOT__linear_dut__DOT__w_outs[__Vi0] = VL_RAND_RESET_I(32);
    }}
    { int __Vi0=0; for (; __Vi0<10; ++__Vi0) {
	    v__DOT__linear_dut__DOT__products[__Vi0] = VL_RAND_RESET_Q(64);
    }}
    v__DOT__linear_dut__DOT__acc_en = VL_RAND_RESET_I(1);
    v__DOT__linear_dut__DOT__acc_rst = VL_RAND_RESET_I(1);
    v__DOT__linear_dut__DOT__genblk1__BRA__0__KET____DOT____Vcellout__w_r__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk1__BRA__1__KET____DOT____Vcellout__w_r__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk1__BRA__2__KET____DOT____Vcellout__w_r__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk1__BRA__3__KET____DOT____Vcellout__w_r__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk1__BRA__4__KET____DOT____Vcellout__w_r__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk1__BRA__5__KET____DOT____Vcellout__w_r__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk1__BRA__6__KET____DOT____Vcellout__w_r__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk1__BRA__7__KET____DOT____Vcellout__w_r__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk1__BRA__8__KET____DOT____Vcellout__w_r__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk1__BRA__9__KET____DOT____Vcellout__w_r__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__d_in = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__d_in = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__d_in = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__d_in = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__d_in = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__d_in = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__d_in = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__d_in = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__d_in = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__d_in = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__w_out = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk3__BRA__0__KET____DOT____Vcellinp__acc__d_in = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk3__BRA__1__KET____DOT____Vcellinp__acc__d_in = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk3__BRA__2__KET____DOT____Vcellinp__acc__d_in = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk3__BRA__3__KET____DOT____Vcellinp__acc__d_in = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk3__BRA__4__KET____DOT____Vcellinp__acc__d_in = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk3__BRA__5__KET____DOT____Vcellinp__acc__d_in = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk3__BRA__6__KET____DOT____Vcellinp__acc__d_in = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk3__BRA__7__KET____DOT____Vcellinp__acc__d_in = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk3__BRA__8__KET____DOT____Vcellinp__acc__d_in = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk3__BRA__9__KET____DOT____Vcellinp__acc__d_in = VL_RAND_RESET_I(32);
    { int __Vi0=0; for (; __Vi0<65; ++__Vi0) {
	    v__DOT__linear_dut__DOT__genblk1__BRA__0__KET____DOT__w_r__DOT__rom[__Vi0] = VL_RAND_RESET_I(32);
    }}
    { int __Vi0=0; for (; __Vi0<65; ++__Vi0) {
	    v__DOT__linear_dut__DOT__genblk1__BRA__1__KET____DOT__w_r__DOT__rom[__Vi0] = VL_RAND_RESET_I(32);
    }}
    { int __Vi0=0; for (; __Vi0<65; ++__Vi0) {
	    v__DOT__linear_dut__DOT__genblk1__BRA__2__KET____DOT__w_r__DOT__rom[__Vi0] = VL_RAND_RESET_I(32);
    }}
    { int __Vi0=0; for (; __Vi0<65; ++__Vi0) {
	    v__DOT__linear_dut__DOT__genblk1__BRA__3__KET____DOT__w_r__DOT__rom[__Vi0] = VL_RAND_RESET_I(32);
    }}
    { int __Vi0=0; for (; __Vi0<65; ++__Vi0) {
	    v__DOT__linear_dut__DOT__genblk1__BRA__4__KET____DOT__w_r__DOT__rom[__Vi0] = VL_RAND_RESET_I(32);
    }}
    { int __Vi0=0; for (; __Vi0<65; ++__Vi0) {
	    v__DOT__linear_dut__DOT__genblk1__BRA__5__KET____DOT__w_r__DOT__rom[__Vi0] = VL_RAND_RESET_I(32);
    }}
    { int __Vi0=0; for (; __Vi0<65; ++__Vi0) {
	    v__DOT__linear_dut__DOT__genblk1__BRA__6__KET____DOT__w_r__DOT__rom[__Vi0] = VL_RAND_RESET_I(32);
    }}
    { int __Vi0=0; for (; __Vi0<65; ++__Vi0) {
	    v__DOT__linear_dut__DOT__genblk1__BRA__7__KET____DOT__w_r__DOT__rom[__Vi0] = VL_RAND_RESET_I(32);
    }}
    { int __Vi0=0; for (; __Vi0<65; ++__Vi0) {
	    v__DOT__linear_dut__DOT__genblk1__BRA__8__KET____DOT__w_r__DOT__rom[__Vi0] = VL_RAND_RESET_I(32);
    }}
    { int __Vi0=0; for (; __Vi0<65; ++__Vi0) {
	    v__DOT__linear_dut__DOT__genblk1__BRA__9__KET____DOT__w_r__DOT__rom[__Vi0] = VL_RAND_RESET_I(32);
    }}
    v__DOT__linear_dut__DOT__genblk3__BRA__0__KET____DOT__acc__DOT__d_acc = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk3__BRA__1__KET____DOT__acc__DOT__d_acc = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk3__BRA__2__KET____DOT__acc__DOT__d_acc = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk3__BRA__3__KET____DOT__acc__DOT__d_acc = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk3__BRA__4__KET____DOT__acc__DOT__d_acc = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk3__BRA__5__KET____DOT__acc__DOT__d_acc = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk3__BRA__6__KET____DOT__acc__DOT__d_acc = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk3__BRA__7__KET____DOT__acc__DOT__d_acc = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk3__BRA__8__KET____DOT__acc__DOT__d_acc = VL_RAND_RESET_I(32);
    v__DOT__linear_dut__DOT__genblk3__BRA__9__KET____DOT__acc__DOT__d_acc = VL_RAND_RESET_I(32);
    __Vclklast__TOP__clk = VL_RAND_RESET_I(1);
    __Vm_traceActivity = VL_RAND_RESET_I(32);
}

void Vlinear::__Vconfigure(Vlinear__Syms* vlSymsp, bool first) {
    if (0 && first) {}  // Prevent unused
    this->__VlSymsp = vlSymsp;
}

Vlinear::~Vlinear() {
    delete __VlSymsp; __VlSymsp=NULL;
}

//--------------------


void Vlinear::eval() {
    Vlinear__Syms* __restrict vlSymsp = this->__VlSymsp; // Setup global symbol table
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Initialize
    if (VL_UNLIKELY(!vlSymsp->__Vm_didInit)) _eval_initial_loop(vlSymsp);
    // Evaluate till stable
    VL_DEBUG_IF(VL_PRINTF("\n----TOP Evaluate Vlinear::eval\n"); );
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	VL_DEBUG_IF(VL_PRINTF(" Clock loop\n"););
	vlSymsp->__Vm_activity = true;
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't converge");
    }
}

void Vlinear::_eval_initial_loop(Vlinear__Syms* __restrict vlSymsp) {
    vlSymsp->__Vm_didInit = true;
    _eval_initial(vlSymsp);
    vlSymsp->__Vm_activity = true;
    int __VclockLoop = 0;
    QData __Vchange=1;
    while (VL_LIKELY(__Vchange)) {
	_eval_settle(vlSymsp);
	_eval(vlSymsp);
	__Vchange = _change_request(vlSymsp);
	if (++__VclockLoop > 100) vl_fatal(__FILE__,__LINE__,__FILE__,"Verilated model didn't DC converge");
    }
}

//--------------------
// Internal Methods

void Vlinear::_initial__TOP__1(Vlinear__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vlinear::_initial__TOP__1\n"); );
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Variables
    //char	__VpadToAlign4[4];
    VL_SIGW(__Vtemp1,127,0,4);
    VL_SIGW(__Vtemp2,127,0,4);
    VL_SIGW(__Vtemp3,127,0,4);
    VL_SIGW(__Vtemp4,127,0,4);
    VL_SIGW(__Vtemp5,127,0,4);
    VL_SIGW(__Vtemp6,127,0,4);
    VL_SIGW(__Vtemp7,127,0,4);
    VL_SIGW(__Vtemp8,127,0,4);
    VL_SIGW(__Vtemp9,127,0,4);
    VL_SIGW(__Vtemp10,127,0,4);
    // Body
    // INITIAL at ../rtl//linear.v:285
    VL_READMEM_Q (true,32,65, 0,2, VL_ULL(0x6f7574332e646174)
		  , vlTOPp->v__DOT__d_outs_prev,0,~0);
    vlTOPp->v__DOT__d_outs_prev[0x40U] = 0x4000000U;
    // INITIAL at ../rtl//linear.v:43
    __Vtemp1[0U] = 0x2e646174U;
    __Vtemp1[1U] = 0x2d303030U;
    __Vtemp1[2U] = 0x79657234U;
    __Vtemp1[3U] = 0x6c61U;
    VL_READMEM_W (true,32,65, 0,4, __Vtemp1, vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__0__KET____DOT__w_r__DOT__rom
		  ,0,~0);
    // INITIAL at ../rtl//linear.v:43
    __Vtemp2[0U] = 0x2e646174U;
    __Vtemp2[1U] = 0x2d303031U;
    __Vtemp2[2U] = 0x79657234U;
    __Vtemp2[3U] = 0x6c61U;
    VL_READMEM_W (true,32,65, 65,4, __Vtemp2, vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__1__KET____DOT__w_r__DOT__rom
		  ,65,~0);
    // INITIAL at ../rtl//linear.v:43
    __Vtemp3[0U] = 0x2e646174U;
    __Vtemp3[1U] = 0x2d303032U;
    __Vtemp3[2U] = 0x79657234U;
    __Vtemp3[3U] = 0x6c61U;
    VL_READMEM_W (true,32,65, 130,4, __Vtemp3, vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__2__KET____DOT__w_r__DOT__rom
		  ,130,~0);
    // INITIAL at ../rtl//linear.v:43
    __Vtemp4[0U] = 0x2e646174U;
    __Vtemp4[1U] = 0x2d303033U;
    __Vtemp4[2U] = 0x79657234U;
    __Vtemp4[3U] = 0x6c61U;
    VL_READMEM_W (true,32,65, 195,4, __Vtemp4, vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__3__KET____DOT__w_r__DOT__rom
		  ,195,~0);
    // INITIAL at ../rtl//linear.v:43
    __Vtemp5[0U] = 0x2e646174U;
    __Vtemp5[1U] = 0x2d303034U;
    __Vtemp5[2U] = 0x79657234U;
    __Vtemp5[3U] = 0x6c61U;
    VL_READMEM_W (true,32,65, 260,4, __Vtemp5, vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__4__KET____DOT__w_r__DOT__rom
		  ,260,~0);
    // INITIAL at ../rtl//linear.v:43
    __Vtemp6[0U] = 0x2e646174U;
    __Vtemp6[1U] = 0x2d303035U;
    __Vtemp6[2U] = 0x79657234U;
    __Vtemp6[3U] = 0x6c61U;
    VL_READMEM_W (true,32,65, 325,4, __Vtemp6, vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__5__KET____DOT__w_r__DOT__rom
		  ,325,~0);
    // INITIAL at ../rtl//linear.v:43
    __Vtemp7[0U] = 0x2e646174U;
    __Vtemp7[1U] = 0x2d303036U;
    __Vtemp7[2U] = 0x79657234U;
    __Vtemp7[3U] = 0x6c61U;
    VL_READMEM_W (true,32,65, 390,4, __Vtemp7, vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__6__KET____DOT__w_r__DOT__rom
		  ,390,~0);
    // INITIAL at ../rtl//linear.v:43
    __Vtemp8[0U] = 0x2e646174U;
    __Vtemp8[1U] = 0x2d303037U;
    __Vtemp8[2U] = 0x79657234U;
    __Vtemp8[3U] = 0x6c61U;
    VL_READMEM_W (true,32,65, 455,4, __Vtemp8, vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__7__KET____DOT__w_r__DOT__rom
		  ,455,~0);
    // INITIAL at ../rtl//linear.v:43
    __Vtemp9[0U] = 0x2e646174U;
    __Vtemp9[1U] = 0x2d303038U;
    __Vtemp9[2U] = 0x79657234U;
    __Vtemp9[3U] = 0x6c61U;
    VL_READMEM_W (true,32,65, 520,4, __Vtemp9, vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__8__KET____DOT__w_r__DOT__rom
		  ,520,~0);
    // INITIAL at ../rtl//linear.v:43
    __Vtemp10[0U] = 0x2e646174U;
    __Vtemp10[1U] = 0x2d303039U;
    __Vtemp10[2U] = 0x79657234U;
    __Vtemp10[3U] = 0x6c61U;
    VL_READMEM_W (true,32,65, 585,4, __Vtemp10, vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__9__KET____DOT__w_r__DOT__rom
		  ,585,~0);
}

VL_INLINE_OPT void Vlinear::_sequent__TOP__2(Vlinear__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vlinear::_sequent__TOP__2\n"); );
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // ALWAYS at ../rtl//linear.v:100
    if (vlTOPp->v__DOT__linear_dut__DOT__acc_en) {
	vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__0__KET____DOT__acc__DOT__d_acc 
	    = ((IData)(vlTOPp->v__DOT__linear_dut__DOT__acc_rst)
	        ? 0U : (vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__0__KET____DOT____Vcellinp__acc__d_in 
			+ vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__0__KET____DOT__acc__DOT__d_acc));
    }
    // ALWAYS at ../rtl//linear.v:100
    if (vlTOPp->v__DOT__linear_dut__DOT__acc_en) {
	vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__1__KET____DOT__acc__DOT__d_acc 
	    = ((IData)(vlTOPp->v__DOT__linear_dut__DOT__acc_rst)
	        ? 0U : (vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__1__KET____DOT____Vcellinp__acc__d_in 
			+ vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__1__KET____DOT__acc__DOT__d_acc));
    }
    // ALWAYS at ../rtl//linear.v:100
    if (vlTOPp->v__DOT__linear_dut__DOT__acc_en) {
	vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__2__KET____DOT__acc__DOT__d_acc 
	    = ((IData)(vlTOPp->v__DOT__linear_dut__DOT__acc_rst)
	        ? 0U : (vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__2__KET____DOT____Vcellinp__acc__d_in 
			+ vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__2__KET____DOT__acc__DOT__d_acc));
    }
    // ALWAYS at ../rtl//linear.v:100
    if (vlTOPp->v__DOT__linear_dut__DOT__acc_en) {
	vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__3__KET____DOT__acc__DOT__d_acc 
	    = ((IData)(vlTOPp->v__DOT__linear_dut__DOT__acc_rst)
	        ? 0U : (vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__3__KET____DOT____Vcellinp__acc__d_in 
			+ vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__3__KET____DOT__acc__DOT__d_acc));
    }
    // ALWAYS at ../rtl//linear.v:100
    if (vlTOPp->v__DOT__linear_dut__DOT__acc_en) {
	vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__4__KET____DOT__acc__DOT__d_acc 
	    = ((IData)(vlTOPp->v__DOT__linear_dut__DOT__acc_rst)
	        ? 0U : (vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__4__KET____DOT____Vcellinp__acc__d_in 
			+ vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__4__KET____DOT__acc__DOT__d_acc));
    }
    // ALWAYS at ../rtl//linear.v:100
    if (vlTOPp->v__DOT__linear_dut__DOT__acc_en) {
	vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__5__KET____DOT__acc__DOT__d_acc 
	    = ((IData)(vlTOPp->v__DOT__linear_dut__DOT__acc_rst)
	        ? 0U : (vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__5__KET____DOT____Vcellinp__acc__d_in 
			+ vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__5__KET____DOT__acc__DOT__d_acc));
    }
    // ALWAYS at ../rtl//linear.v:100
    if (vlTOPp->v__DOT__linear_dut__DOT__acc_en) {
	vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__6__KET____DOT__acc__DOT__d_acc 
	    = ((IData)(vlTOPp->v__DOT__linear_dut__DOT__acc_rst)
	        ? 0U : (vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__6__KET____DOT____Vcellinp__acc__d_in 
			+ vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__6__KET____DOT__acc__DOT__d_acc));
    }
    // ALWAYS at ../rtl//linear.v:100
    if (vlTOPp->v__DOT__linear_dut__DOT__acc_en) {
	vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__7__KET____DOT__acc__DOT__d_acc 
	    = ((IData)(vlTOPp->v__DOT__linear_dut__DOT__acc_rst)
	        ? 0U : (vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__7__KET____DOT____Vcellinp__acc__d_in 
			+ vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__7__KET____DOT__acc__DOT__d_acc));
    }
    // ALWAYS at ../rtl//linear.v:100
    if (vlTOPp->v__DOT__linear_dut__DOT__acc_en) {
	vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__8__KET____DOT__acc__DOT__d_acc 
	    = ((IData)(vlTOPp->v__DOT__linear_dut__DOT__acc_rst)
	        ? 0U : (vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__8__KET____DOT____Vcellinp__acc__d_in 
			+ vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__8__KET____DOT__acc__DOT__d_acc));
    }
    // ALWAYS at ../rtl//linear.v:100
    if (vlTOPp->v__DOT__linear_dut__DOT__acc_en) {
	vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__9__KET____DOT__acc__DOT__d_acc 
	    = ((IData)(vlTOPp->v__DOT__linear_dut__DOT__acc_rst)
	        ? 0U : (vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__9__KET____DOT____Vcellinp__acc__d_in 
			+ vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__9__KET____DOT__acc__DOT__d_acc));
    }
    // ALWAYS at ../rtl//linear.v:228
    if (vlTOPp->rst) {
	vlTOPp->v__DOT__linear_dut__DOT__acc_en = 1U;
	vlTOPp->v__DOT__linear_dut__DOT__acc_rst = 0U;
	vlTOPp->busy = 0U;
	vlTOPp->v__DOT__linear_dut__DOT__addr = 0U;
	vlTOPp->done = 0U;
    } else {
	if ((((IData)(vlTOPp->start) & (~ (IData)(vlTOPp->busy))) 
	     & (~ (IData)(vlTOPp->done)))) {
	    vlTOPp->busy = 1U;
	    vlTOPp->v__DOT__linear_dut__DOT__acc_en = 1U;
	    vlTOPp->v__DOT__linear_dut__DOT__acc_rst = 0U;
	    vlTOPp->v__DOT__linear_dut__DOT__addr = 0U;
	    vlTOPp->done = 0U;
	} else {
	    if (((IData)(vlTOPp->busy) & (0x40U > vlTOPp->v__DOT__linear_dut__DOT__addr))) {
		vlTOPp->v__DOT__linear_dut__DOT__addr 
		    = ((IData)(1U) + vlTOPp->v__DOT__linear_dut__DOT__addr);
		vlTOPp->busy = 1U;
		vlTOPp->v__DOT__linear_dut__DOT__acc_en = 1U;
		vlTOPp->v__DOT__linear_dut__DOT__acc_rst = 0U;
		vlTOPp->done = 0U;
	    } else {
		if (((IData)(vlTOPp->busy) & (0x40U 
					      <= vlTOPp->v__DOT__linear_dut__DOT__addr))) {
		    vlTOPp->busy = 0U;
		    vlTOPp->v__DOT__linear_dut__DOT__acc_en = 0U;
		    vlTOPp->v__DOT__linear_dut__DOT__acc_rst = 0U;
		    vlTOPp->v__DOT__linear_dut__DOT__addr = 0U;
		    vlTOPp->done = 1U;
		}
	    }
	}
    }
}

void Vlinear::_initial__TOP__3(Vlinear__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vlinear::_initial__TOP__3\n"); );
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // INITIAL at ../rtl//linear.v:96
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__0__KET____DOT__acc__DOT__d_acc = 0U;
    // INITIAL at ../rtl//linear.v:96
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__1__KET____DOT__acc__DOT__d_acc = 0U;
    // INITIAL at ../rtl//linear.v:96
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__2__KET____DOT__acc__DOT__d_acc = 0U;
    // INITIAL at ../rtl//linear.v:96
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__3__KET____DOT__acc__DOT__d_acc = 0U;
    // INITIAL at ../rtl//linear.v:96
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__4__KET____DOT__acc__DOT__d_acc = 0U;
    // INITIAL at ../rtl//linear.v:96
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__5__KET____DOT__acc__DOT__d_acc = 0U;
    // INITIAL at ../rtl//linear.v:96
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__6__KET____DOT__acc__DOT__d_acc = 0U;
    // INITIAL at ../rtl//linear.v:96
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__7__KET____DOT__acc__DOT__d_acc = 0U;
    // INITIAL at ../rtl//linear.v:96
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__8__KET____DOT__acc__DOT__d_acc = 0U;
    // INITIAL at ../rtl//linear.v:202
    vlTOPp->v__DOT__linear_dut__DOT__acc_en = 0U;
    vlTOPp->v__DOT__linear_dut__DOT__acc_rst = 0U;
    // INITIAL at ../rtl//linear.v:96
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__9__KET____DOT__acc__DOT__d_acc = 0U;
    // INITIAL at ../rtl//linear.v:222
    vlTOPp->busy = 0U;
    vlTOPp->done = 0U;
    // INITIAL at ../rtl//linear.v:158
    vlTOPp->v__DOT__linear_dut__DOT__addr = 0U;
}

VL_INLINE_OPT void Vlinear::_sequent__TOP__4(Vlinear__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vlinear::_sequent__TOP__4\n"); );
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->d_outs[0U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__0__KET____DOT__acc__DOT__d_acc;
    vlTOPp->d_outs[1U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__1__KET____DOT__acc__DOT__d_acc;
    vlTOPp->d_outs[2U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__2__KET____DOT__acc__DOT__d_acc;
    vlTOPp->d_outs[3U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__3__KET____DOT__acc__DOT__d_acc;
    vlTOPp->d_outs[4U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__4__KET____DOT__acc__DOT__d_acc;
    vlTOPp->d_outs[5U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__5__KET____DOT__acc__DOT__d_acc;
    vlTOPp->d_outs[6U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__6__KET____DOT__acc__DOT__d_acc;
    vlTOPp->d_outs[7U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__7__KET____DOT__acc__DOT__d_acc;
    vlTOPp->d_outs[8U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__8__KET____DOT__acc__DOT__d_acc;
    vlTOPp->d_outs[9U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__9__KET____DOT__acc__DOT__d_acc;
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__0__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__0__KET____DOT__w_r__DOT__rom
	   [(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__1__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0xffU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__1__KET____DOT__w_r__DOT__rom
	   [(0xffU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__2__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0xffU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__2__KET____DOT__w_r__DOT__rom
	   [(0xffU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__3__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0x1ffU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__3__KET____DOT__w_r__DOT__rom
	   [(0x1ffU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__4__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0x1ffU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__4__KET____DOT__w_r__DOT__rom
	   [(0x1ffU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__5__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0x1ffU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__5__KET____DOT__w_r__DOT__rom
	   [(0x1ffU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__6__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0x1ffU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__6__KET____DOT__w_r__DOT__rom
	   [(0x1ffU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__7__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0x3ffU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__7__KET____DOT__w_r__DOT__rom
	   [(0x3ffU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__8__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0x3ffU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__8__KET____DOT__w_r__DOT__rom
	   [(0x3ffU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__9__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0x3ffU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__9__KET____DOT__w_r__DOT__rom
	   [(0x3ffU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
}

void Vlinear::_settle__TOP__5(Vlinear__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vlinear::_settle__TOP__5\n"); );
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->d_outs[0U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__0__KET____DOT__acc__DOT__d_acc;
    vlTOPp->d_outs[1U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__1__KET____DOT__acc__DOT__d_acc;
    vlTOPp->d_outs[2U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__2__KET____DOT__acc__DOT__d_acc;
    vlTOPp->d_outs[3U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__3__KET____DOT__acc__DOT__d_acc;
    vlTOPp->d_outs[4U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__4__KET____DOT__acc__DOT__d_acc;
    vlTOPp->d_outs[5U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__5__KET____DOT__acc__DOT__d_acc;
    vlTOPp->d_outs[6U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__6__KET____DOT__acc__DOT__d_acc;
    vlTOPp->d_outs[7U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__7__KET____DOT__acc__DOT__d_acc;
    vlTOPp->d_outs[8U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__8__KET____DOT__acc__DOT__d_acc;
    vlTOPp->d_outs[9U] = vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__9__KET____DOT__acc__DOT__d_acc;
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__d_in 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__d_outs_prev[(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__0__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__0__KET____DOT__w_r__DOT__rom
	   [(0x7fU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__1__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0xffU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__1__KET____DOT__w_r__DOT__rom
	   [(0xffU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__2__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0xffU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__2__KET____DOT__w_r__DOT__rom
	   [(0xffU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__3__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0x1ffU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__3__KET____DOT__w_r__DOT__rom
	   [(0x1ffU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__4__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0x1ffU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__4__KET____DOT__w_r__DOT__rom
	   [(0x1ffU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__5__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0x1ffU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__5__KET____DOT__w_r__DOT__rom
	   [(0x1ffU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__6__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0x1ffU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__6__KET____DOT__w_r__DOT__rom
	   [(0x1ffU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__7__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0x3ffU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__7__KET____DOT__w_r__DOT__rom
	   [(0x3ffU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__8__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0x3ffU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__8__KET____DOT__w_r__DOT__rom
	   [(0x3ffU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__9__KET____DOT____Vcellout__w_r__w_out 
	= ((0x40U >= (0x3ffU & vlTOPp->v__DOT__linear_dut__DOT__addr))
	    ? vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__9__KET____DOT__w_r__DOT__rom
	   [(0x3ffU & vlTOPp->v__DOT__linear_dut__DOT__addr)]
	    : 0U);
    vlTOPp->v__DOT__d_outs[9U] = vlTOPp->d_outs[9U];
    vlTOPp->v__DOT__d_outs[8U] = vlTOPp->d_outs[8U];
    vlTOPp->v__DOT__d_outs[7U] = vlTOPp->d_outs[7U];
    vlTOPp->v__DOT__d_outs[6U] = vlTOPp->d_outs[6U];
    vlTOPp->v__DOT__d_outs[5U] = vlTOPp->d_outs[5U];
    vlTOPp->v__DOT__d_outs[4U] = vlTOPp->d_outs[4U];
    vlTOPp->v__DOT__d_outs[3U] = vlTOPp->d_outs[3U];
    vlTOPp->v__DOT__d_outs[2U] = vlTOPp->d_outs[2U];
    vlTOPp->v__DOT__d_outs[1U] = vlTOPp->d_outs[1U];
    vlTOPp->v__DOT__d_outs[0U] = vlTOPp->d_outs[0U];
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[0U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__0__KET____DOT____Vcellout__w_r__w_out;
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[1U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__1__KET____DOT____Vcellout__w_r__w_out;
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[2U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__2__KET____DOT____Vcellout__w_r__w_out;
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[3U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__3__KET____DOT____Vcellout__w_r__w_out;
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[4U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__4__KET____DOT____Vcellout__w_r__w_out;
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[5U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__5__KET____DOT____Vcellout__w_r__w_out;
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[6U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__6__KET____DOT____Vcellout__w_r__w_out;
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[7U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__7__KET____DOT____Vcellout__w_r__w_out;
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[8U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__8__KET____DOT____Vcellout__w_r__w_out;
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[9U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__9__KET____DOT____Vcellout__w_r__w_out;
}

VL_INLINE_OPT void Vlinear::_sequent__TOP__6(Vlinear__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vlinear::_sequent__TOP__6\n"); );
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__d_outs[9U] = vlTOPp->d_outs[9U];
    vlTOPp->v__DOT__d_outs[8U] = vlTOPp->d_outs[8U];
    vlTOPp->v__DOT__d_outs[7U] = vlTOPp->d_outs[7U];
    vlTOPp->v__DOT__d_outs[6U] = vlTOPp->d_outs[6U];
    vlTOPp->v__DOT__d_outs[5U] = vlTOPp->d_outs[5U];
    vlTOPp->v__DOT__d_outs[4U] = vlTOPp->d_outs[4U];
    vlTOPp->v__DOT__d_outs[3U] = vlTOPp->d_outs[3U];
    vlTOPp->v__DOT__d_outs[2U] = vlTOPp->d_outs[2U];
    vlTOPp->v__DOT__d_outs[1U] = vlTOPp->d_outs[1U];
    vlTOPp->v__DOT__d_outs[0U] = vlTOPp->d_outs[0U];
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[0U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__0__KET____DOT____Vcellout__w_r__w_out;
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[1U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__1__KET____DOT____Vcellout__w_r__w_out;
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[2U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__2__KET____DOT____Vcellout__w_r__w_out;
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[3U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__3__KET____DOT____Vcellout__w_r__w_out;
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[4U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__4__KET____DOT____Vcellout__w_r__w_out;
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[5U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__5__KET____DOT____Vcellout__w_r__w_out;
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[6U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__6__KET____DOT____Vcellout__w_r__w_out;
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[7U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__7__KET____DOT____Vcellout__w_r__w_out;
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[8U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__8__KET____DOT____Vcellout__w_r__w_out;
    vlTOPp->v__DOT__linear_dut__DOT__w_outs[9U] = vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__9__KET____DOT____Vcellout__w_r__w_out;
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[9U] = vlTOPp->v__DOT__d_outs
	[9U];
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[8U] = vlTOPp->v__DOT__d_outs
	[8U];
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[7U] = vlTOPp->v__DOT__d_outs
	[7U];
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[6U] = vlTOPp->v__DOT__d_outs
	[6U];
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[5U] = vlTOPp->v__DOT__d_outs
	[5U];
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[4U] = vlTOPp->v__DOT__d_outs
	[4U];
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[3U] = vlTOPp->v__DOT__d_outs
	[3U];
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[2U] = vlTOPp->v__DOT__d_outs
	[2U];
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[1U] = vlTOPp->v__DOT__d_outs
	[1U];
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[0U] = vlTOPp->v__DOT__d_outs
	[0U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[0U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[1U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[2U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[3U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[4U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[5U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[6U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[7U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[8U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[9U];
}

void Vlinear::_settle__TOP__7(Vlinear__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vlinear::_settle__TOP__7\n"); );
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[9U] = vlTOPp->v__DOT__d_outs
	[9U];
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[8U] = vlTOPp->v__DOT__d_outs
	[8U];
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[7U] = vlTOPp->v__DOT__d_outs
	[7U];
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[6U] = vlTOPp->v__DOT__d_outs
	[6U];
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[5U] = vlTOPp->v__DOT__d_outs
	[5U];
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[4U] = vlTOPp->v__DOT__d_outs
	[4U];
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[3U] = vlTOPp->v__DOT__d_outs
	[3U];
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[2U] = vlTOPp->v__DOT__d_outs
	[2U];
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[1U] = vlTOPp->v__DOT__d_outs
	[1U];
    vlTOPp->v__DOT__linear_dut__DOT__d_outs[0U] = vlTOPp->v__DOT__d_outs
	[0U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[0U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[1U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[2U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[3U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[4U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[5U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[6U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[7U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[8U];
    vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__w_out 
	= vlTOPp->v__DOT__linear_dut__DOT__w_outs[9U];
    vlTOPp->v__DOT__linear_dut__DOT__products[0U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__d_in));
    vlTOPp->v__DOT__linear_dut__DOT__products[1U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__d_in));
    vlTOPp->v__DOT__linear_dut__DOT__products[2U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__d_in));
    vlTOPp->v__DOT__linear_dut__DOT__products[3U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__d_in));
    vlTOPp->v__DOT__linear_dut__DOT__products[4U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__d_in));
    vlTOPp->v__DOT__linear_dut__DOT__products[5U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__d_in));
    vlTOPp->v__DOT__linear_dut__DOT__products[6U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__d_in));
    vlTOPp->v__DOT__linear_dut__DOT__products[7U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__d_in));
    vlTOPp->v__DOT__linear_dut__DOT__products[8U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__d_in));
    vlTOPp->v__DOT__linear_dut__DOT__products[9U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__d_in));
}

VL_INLINE_OPT void Vlinear::_sequent__TOP__8(Vlinear__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vlinear::_sequent__TOP__8\n"); );
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__linear_dut__DOT__products[0U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__d_in));
    vlTOPp->v__DOT__linear_dut__DOT__products[1U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__d_in));
    vlTOPp->v__DOT__linear_dut__DOT__products[2U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__d_in));
    vlTOPp->v__DOT__linear_dut__DOT__products[3U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__d_in));
    vlTOPp->v__DOT__linear_dut__DOT__products[4U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__d_in));
    vlTOPp->v__DOT__linear_dut__DOT__products[5U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__d_in));
    vlTOPp->v__DOT__linear_dut__DOT__products[6U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__d_in));
    vlTOPp->v__DOT__linear_dut__DOT__products[7U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__d_in));
    vlTOPp->v__DOT__linear_dut__DOT__products[8U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__d_in));
    vlTOPp->v__DOT__linear_dut__DOT__products[9U] = 
	VL_MULS_QQQ(64,64,64, VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__w_out), 
		    VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__d_in));
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__0__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [0U] >> 0x1aU));
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__1__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [1U] >> 0x1aU));
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__2__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [2U] >> 0x1aU));
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__3__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [3U] >> 0x1aU));
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__4__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [4U] >> 0x1aU));
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__5__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [5U] >> 0x1aU));
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__6__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [6U] >> 0x1aU));
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__7__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [7U] >> 0x1aU));
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__8__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [8U] >> 0x1aU));
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__9__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [9U] >> 0x1aU));
}

void Vlinear::_settle__TOP__9(Vlinear__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vlinear::_settle__TOP__9\n"); );
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__0__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [0U] >> 0x1aU));
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__1__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [1U] >> 0x1aU));
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__2__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [2U] >> 0x1aU));
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__3__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [3U] >> 0x1aU));
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__4__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [4U] >> 0x1aU));
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__5__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [5U] >> 0x1aU));
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__6__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [6U] >> 0x1aU));
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__7__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [7U] >> 0x1aU));
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__8__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [8U] >> 0x1aU));
    vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__9__KET____DOT____Vcellinp__acc__d_in 
	= (IData)((vlTOPp->v__DOT__linear_dut__DOT__products
		   [9U] >> 0x1aU));
}

void Vlinear::_eval(Vlinear__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vlinear::_eval\n"); );
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    if (((IData)(vlTOPp->clk) & (~ (IData)(vlTOPp->__Vclklast__TOP__clk)))) {
	vlTOPp->_sequent__TOP__2(vlSymsp);
	vlTOPp->__Vm_traceActivity = (2U | vlTOPp->__Vm_traceActivity);
	vlTOPp->_sequent__TOP__4(vlSymsp);
	vlTOPp->_sequent__TOP__6(vlSymsp);
	vlTOPp->_sequent__TOP__8(vlSymsp);
    }
    // Final
    vlTOPp->__Vclklast__TOP__clk = vlTOPp->clk;
}

void Vlinear::_eval_initial(Vlinear__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vlinear::_eval_initial\n"); );
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_initial__TOP__1(vlSymsp);
    vlTOPp->__Vm_traceActivity = (1U | vlTOPp->__Vm_traceActivity);
    vlTOPp->_initial__TOP__3(vlSymsp);
}

void Vlinear::final() {
    VL_DEBUG_IF(VL_PRINTF("    Vlinear::final\n"); );
    // Variables
    Vlinear__Syms* __restrict vlSymsp = this->__VlSymsp;
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
}

void Vlinear::_eval_settle(Vlinear__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vlinear::_eval_settle\n"); );
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    vlTOPp->_settle__TOP__5(vlSymsp);
    vlTOPp->__Vm_traceActivity = (1U | vlTOPp->__Vm_traceActivity);
    vlTOPp->_settle__TOP__7(vlSymsp);
    vlTOPp->_settle__TOP__9(vlSymsp);
}

VL_INLINE_OPT QData Vlinear::_change_request(Vlinear__Syms* __restrict vlSymsp) {
    VL_DEBUG_IF(VL_PRINTF("    Vlinear::_change_request\n"); );
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    // Body
    // Change detection
    QData __req = false;  // Logically a bool
    return __req;
}
