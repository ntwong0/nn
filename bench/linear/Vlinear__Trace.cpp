// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "Vlinear__Syms.h"


//======================

void Vlinear::traceChg(VerilatedVcd* vcdp, void* userthis, uint32_t code) {
    // Callback from vcd->dump()
    Vlinear* t=(Vlinear*)userthis;
    Vlinear__Syms* __restrict vlSymsp = t->__VlSymsp; // Setup global symbol table
    if (vlSymsp->getClearActivity()) {
	t->traceChgThis (vlSymsp, vcdp, code);
    }
}

//======================


void Vlinear::traceChgThis(Vlinear__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	if (VL_UNLIKELY((1U & (vlTOPp->__Vm_traceActivity 
			       | (vlTOPp->__Vm_traceActivity 
				  >> 1U))))) {
	    vlTOPp->traceChgThis__2(vlSymsp, vcdp, code);
	}
	vlTOPp->traceChgThis__3(vlSymsp, vcdp, code);
    }
    // Final
    vlTOPp->__Vm_traceActivity = 0U;
}

void Vlinear::traceChgThis__2(Vlinear__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->chgBus  (c+1,(vlTOPp->v__DOT__d_outs[0]),32);
	vcdp->chgBus  (c+2,(vlTOPp->v__DOT__d_outs[1]),32);
	vcdp->chgBus  (c+3,(vlTOPp->v__DOT__d_outs[2]),32);
	vcdp->chgBus  (c+4,(vlTOPp->v__DOT__d_outs[3]),32);
	vcdp->chgBus  (c+5,(vlTOPp->v__DOT__d_outs[4]),32);
	vcdp->chgBus  (c+6,(vlTOPp->v__DOT__d_outs[5]),32);
	vcdp->chgBus  (c+7,(vlTOPp->v__DOT__d_outs[6]),32);
	vcdp->chgBus  (c+8,(vlTOPp->v__DOT__d_outs[7]),32);
	vcdp->chgBus  (c+9,(vlTOPp->v__DOT__d_outs[8]),32);
	vcdp->chgBus  (c+10,(vlTOPp->v__DOT__d_outs[9]),32);
	vcdp->chgBus  (c+11,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[0]),32);
	vcdp->chgBus  (c+12,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[1]),32);
	vcdp->chgBus  (c+13,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[2]),32);
	vcdp->chgBus  (c+14,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[3]),32);
	vcdp->chgBus  (c+15,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[4]),32);
	vcdp->chgBus  (c+16,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[5]),32);
	vcdp->chgBus  (c+17,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[6]),32);
	vcdp->chgBus  (c+18,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[7]),32);
	vcdp->chgBus  (c+19,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[8]),32);
	vcdp->chgBus  (c+20,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[9]),32);
	vcdp->chgBus  (c+21,(vlTOPp->v__DOT__linear_dut__DOT__addr),32);
	vcdp->chgBus  (c+22,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[0]),32);
	vcdp->chgBus  (c+23,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[1]),32);
	vcdp->chgBus  (c+24,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[2]),32);
	vcdp->chgBus  (c+25,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[3]),32);
	vcdp->chgBus  (c+26,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[4]),32);
	vcdp->chgBus  (c+27,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[5]),32);
	vcdp->chgBus  (c+28,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[6]),32);
	vcdp->chgBus  (c+29,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[7]),32);
	vcdp->chgBus  (c+30,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[8]),32);
	vcdp->chgBus  (c+31,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[9]),32);
	vcdp->chgQuad (c+32,(vlTOPp->v__DOT__linear_dut__DOT__products[0]),64);
	vcdp->chgQuad (c+34,(vlTOPp->v__DOT__linear_dut__DOT__products[1]),64);
	vcdp->chgQuad (c+36,(vlTOPp->v__DOT__linear_dut__DOT__products[2]),64);
	vcdp->chgQuad (c+38,(vlTOPp->v__DOT__linear_dut__DOT__products[3]),64);
	vcdp->chgQuad (c+40,(vlTOPp->v__DOT__linear_dut__DOT__products[4]),64);
	vcdp->chgQuad (c+42,(vlTOPp->v__DOT__linear_dut__DOT__products[5]),64);
	vcdp->chgQuad (c+44,(vlTOPp->v__DOT__linear_dut__DOT__products[6]),64);
	vcdp->chgQuad (c+46,(vlTOPp->v__DOT__linear_dut__DOT__products[7]),64);
	vcdp->chgQuad (c+48,(vlTOPp->v__DOT__linear_dut__DOT__products[8]),64);
	vcdp->chgQuad (c+50,(vlTOPp->v__DOT__linear_dut__DOT__products[9]),64);
	vcdp->chgBus  (c+54,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__0__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->chgBus  (c+55,(((IData)(0x41U) + vlTOPp->v__DOT__linear_dut__DOT__addr)),32);
	vcdp->chgBus  (c+56,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__1__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->chgBus  (c+57,(((IData)(0x82U) + vlTOPp->v__DOT__linear_dut__DOT__addr)),32);
	vcdp->chgBus  (c+58,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__2__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->chgBus  (c+59,(((IData)(0xc3U) + vlTOPp->v__DOT__linear_dut__DOT__addr)),32);
	vcdp->chgBus  (c+60,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__3__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->chgBus  (c+61,(((IData)(0x104U) + vlTOPp->v__DOT__linear_dut__DOT__addr)),32);
	vcdp->chgBus  (c+62,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__4__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->chgBus  (c+63,(((IData)(0x145U) + vlTOPp->v__DOT__linear_dut__DOT__addr)),32);
	vcdp->chgBus  (c+64,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__5__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->chgBus  (c+65,(((IData)(0x186U) + vlTOPp->v__DOT__linear_dut__DOT__addr)),32);
	vcdp->chgBus  (c+66,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__6__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->chgBus  (c+67,(((IData)(0x1c7U) + vlTOPp->v__DOT__linear_dut__DOT__addr)),32);
	vcdp->chgBus  (c+68,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__7__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->chgBus  (c+69,(((IData)(0x208U) + vlTOPp->v__DOT__linear_dut__DOT__addr)),32);
	vcdp->chgBus  (c+70,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__8__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->chgBus  (c+71,(((IData)(0x249U) + vlTOPp->v__DOT__linear_dut__DOT__addr)),32);
	vcdp->chgBus  (c+72,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__9__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->chgBus  (c+73,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->chgBus  (c+74,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->chgQuad (c+75,(VL_MULS_QQQ(64,64,64, 
					 VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__w_out), 
					 VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->chgBus  (c+77,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->chgBus  (c+78,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->chgQuad (c+79,(VL_MULS_QQQ(64,64,64, 
					 VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__w_out), 
					 VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->chgBus  (c+81,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->chgBus  (c+82,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->chgQuad (c+83,(VL_MULS_QQQ(64,64,64, 
					 VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__w_out), 
					 VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->chgBus  (c+85,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->chgBus  (c+86,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->chgQuad (c+87,(VL_MULS_QQQ(64,64,64, 
					 VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__w_out), 
					 VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->chgBus  (c+89,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->chgBus  (c+90,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->chgQuad (c+91,(VL_MULS_QQQ(64,64,64, 
					 VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__w_out), 
					 VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->chgBus  (c+93,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->chgBus  (c+94,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->chgQuad (c+95,(VL_MULS_QQQ(64,64,64, 
					 VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__w_out), 
					 VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->chgBus  (c+97,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->chgBus  (c+98,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->chgQuad (c+99,(VL_MULS_QQQ(64,64,64, 
					 VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__w_out), 
					 VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->chgBus  (c+101,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->chgBus  (c+102,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->chgQuad (c+103,(VL_MULS_QQQ(64,64,64, 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__w_out), 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->chgBus  (c+105,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->chgBus  (c+106,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->chgQuad (c+107,(VL_MULS_QQQ(64,64,64, 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__w_out), 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->chgBus  (c+109,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->chgBus  (c+110,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->chgQuad (c+111,(VL_MULS_QQQ(64,64,64, 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__w_out), 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->chgBus  (c+113,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__0__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->chgBus  (c+114,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__0__KET____DOT__acc__DOT__d_acc),32);
	vcdp->chgBus  (c+115,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__1__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->chgBus  (c+116,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__1__KET____DOT__acc__DOT__d_acc),32);
	vcdp->chgBus  (c+117,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__2__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->chgBus  (c+118,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__2__KET____DOT__acc__DOT__d_acc),32);
	vcdp->chgBus  (c+119,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__3__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->chgBus  (c+120,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__3__KET____DOT__acc__DOT__d_acc),32);
	vcdp->chgBus  (c+121,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__4__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->chgBus  (c+122,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__4__KET____DOT__acc__DOT__d_acc),32);
	vcdp->chgBus  (c+123,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__5__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->chgBus  (c+124,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__5__KET____DOT__acc__DOT__d_acc),32);
	vcdp->chgBus  (c+125,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__6__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->chgBus  (c+126,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__6__KET____DOT__acc__DOT__d_acc),32);
	vcdp->chgBus  (c+127,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__7__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->chgBus  (c+128,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__7__KET____DOT__acc__DOT__d_acc),32);
	vcdp->chgBus  (c+129,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__8__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->chgBus  (c+130,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__8__KET____DOT__acc__DOT__d_acc),32);
	vcdp->chgBus  (c+131,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__9__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->chgBus  (c+132,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__9__KET____DOT__acc__DOT__d_acc),32);
	vcdp->chgBit  (c+52,(vlTOPp->v__DOT__linear_dut__DOT__acc_en));
	vcdp->chgBit  (c+53,(vlTOPp->v__DOT__linear_dut__DOT__acc_rst));
    }
}

void Vlinear::traceChgThis__3(Vlinear__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vcdp->chgBus  (c+133,(vlTOPp->d_outs[0]),32);
	vcdp->chgBus  (c+134,(vlTOPp->d_outs[1]),32);
	vcdp->chgBus  (c+135,(vlTOPp->d_outs[2]),32);
	vcdp->chgBus  (c+136,(vlTOPp->d_outs[3]),32);
	vcdp->chgBus  (c+137,(vlTOPp->d_outs[4]),32);
	vcdp->chgBus  (c+138,(vlTOPp->d_outs[5]),32);
	vcdp->chgBus  (c+139,(vlTOPp->d_outs[6]),32);
	vcdp->chgBus  (c+140,(vlTOPp->d_outs[7]),32);
	vcdp->chgBus  (c+141,(vlTOPp->d_outs[8]),32);
	vcdp->chgBus  (c+142,(vlTOPp->d_outs[9]),32);
	vcdp->chgBit  (c+144,(vlTOPp->start));
	vcdp->chgBit  (c+145,(vlTOPp->rst));
	vcdp->chgBit  (c+146,(vlTOPp->busy));
	vcdp->chgBit  (c+147,(vlTOPp->done));
	vcdp->chgBit  (c+143,(vlTOPp->clk));
    }
}
