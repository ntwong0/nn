// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Tracing implementation internals
#include "verilated_vcd_c.h"
#include "Vlinear__Syms.h"


//======================

void Vlinear::trace (VerilatedVcdC* tfp, int, int) {
    tfp->spTrace()->addCallback (&Vlinear::traceInit, &Vlinear::traceFull, &Vlinear::traceChg, this);
}
void Vlinear::traceInit(VerilatedVcd* vcdp, void* userthis, uint32_t code) {
    // Callback from vcd->open()
    Vlinear* t=(Vlinear*)userthis;
    Vlinear__Syms* __restrict vlSymsp = t->__VlSymsp; // Setup global symbol table
    if (!Verilated::calcUnusedSigs()) vl_fatal(__FILE__,__LINE__,__FILE__,"Turning on wave traces requires Verilated::traceEverOn(true) call before time 0.");
    vcdp->scopeEscape(' ');
    t->traceInitThis (vlSymsp, vcdp, code);
    vcdp->scopeEscape('.');
}
void Vlinear::traceFull(VerilatedVcd* vcdp, void* userthis, uint32_t code) {
    // Callback from vcd->dump()
    Vlinear* t=(Vlinear*)userthis;
    Vlinear__Syms* __restrict vlSymsp = t->__VlSymsp; // Setup global symbol table
    t->traceFullThis (vlSymsp, vcdp, code);
}

//======================


void Vlinear::traceInitThis(Vlinear__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    vcdp->module(vlSymsp->name()); // Setup signal names
    // Body
    {
	vlTOPp->traceInitThis__1(vlSymsp, vcdp, code);
    }
}

void Vlinear::traceFullThis(Vlinear__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	vlTOPp->traceFullThis__1(vlSymsp, vcdp, code);
    }
    // Final
    vlTOPp->__Vm_traceActivity = 0U;
}

void Vlinear::traceInitThis__1(Vlinear__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Body
    {
	{int i; for (i=0; i<10; i++) {
		vcdp->declBus  (c+133+i*1,"d_outs",(i+0),31,0);}}
	vcdp->declBit  (c+143,"clk",-1);
	vcdp->declBit  (c+144,"start",-1);
	vcdp->declBit  (c+145,"rst",-1);
	vcdp->declBit  (c+146,"busy",-1);
	vcdp->declBit  (c+147,"done",-1);
	vcdp->declBus  (c+148,"v IN_CHANNELS",-1,31,0);
	vcdp->declBus  (c+149,"v OUT_CHANNELS",-1,31,0);
	vcdp->declBus  (c+150,"v DATA_WIDTH",-1,31,0);
	{int i; for (i=0; i<10; i++) {
		vcdp->declBus  (c+1+i*1,"v d_outs",(i+0),31,0);}}
	vcdp->declBit  (c+143,"v clk",-1);
	vcdp->declBit  (c+144,"v start",-1);
	vcdp->declBit  (c+145,"v rst",-1);
	vcdp->declBit  (c+146,"v busy",-1);
	vcdp->declBit  (c+147,"v done",-1);
	// Tracing: v d_outs_prev // Ignored: Wide memory > --trace-max-array ents at ../rtl//linear.v:283
	vcdp->declQuad (c+151,"v linear_dut FN_PREFIX",-1,47,0);
	vcdp->declBus  (c+148,"v linear_dut IN_CHANNELS",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut OUT_CHANNELS",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut DATA_WIDTH",-1,31,0);
	// Tracing: v linear_dut d_outs_prev // Ignored: Wide memory > --trace-max-array ents at ../rtl//linear.v:146
	{int i; for (i=0; i<10; i++) {
		vcdp->declBus  (c+11+i*1,"v linear_dut d_outs",(i+0),31,0);}}
	vcdp->declBit  (c+143,"v linear_dut clk",-1);
	vcdp->declBit  (c+144,"v linear_dut start",-1);
	vcdp->declBit  (c+145,"v linear_dut rst",-1);
	vcdp->declBit  (c+146,"v linear_dut busy",-1);
	vcdp->declBit  (c+147,"v linear_dut done",-1);
	vcdp->declBus  (c+21,"v linear_dut addr",-1,31,0);
	{int i; for (i=0; i<10; i++) {
		vcdp->declBus  (c+22+i*1,"v linear_dut w_outs",(i+0),31,0);}}
	// Tracing: v linear_dut i // Ignored: Verilator trace_off at ../rtl//linear.v:161
	{int i; for (i=0; i<10; i++) {
		vcdp->declQuad (c+32+i*2,"v linear_dut products",(i+0),63,0);}}
	vcdp->declBit  (c+52,"v linear_dut acc_en",-1);
	vcdp->declBit  (c+53,"v linear_dut acc_rst",-1);
	vcdp->declBus  (c+148,"v linear_dut genblk1[0] w_r IN_CHANNELS",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk1[0] w_r OUT_CHANNELS",-1,31,0);
	vcdp->declQuad (c+151,"v linear_dut genblk1[0] w_r FN_PREFIX",-1,47,0);
	vcdp->declBus  (c+153,"v linear_dut genblk1[0] w_r INDEX",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut genblk1[0] w_r DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+21,"v linear_dut genblk1[0] w_r addr",-1,31,0);
	vcdp->declBus  (c+54,"v linear_dut genblk1[0] w_r w_out",-1,31,0);
	vcdp->declArray(c+154,"v linear_dut genblk1[0] w_r FILENAME",-1,111,0);
	vcdp->declBus  (c+158,"v linear_dut genblk1[0] w_r SIZE",-1,31,0);
	vcdp->declBus  (c+153,"v linear_dut genblk1[0] w_r START_INDEX",-1,31,0);
	vcdp->declBus  (c+159,"v linear_dut genblk1[0] w_r END_INDEX",-1,31,0);
	// Tracing: v linear_dut genblk1[0] w_r rom // Ignored: Wide memory > --trace-max-array ents at ../rtl//linear.v:41
	vcdp->declBus  (c+148,"v linear_dut genblk1[1] w_r IN_CHANNELS",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk1[1] w_r OUT_CHANNELS",-1,31,0);
	vcdp->declQuad (c+151,"v linear_dut genblk1[1] w_r FN_PREFIX",-1,47,0);
	vcdp->declBus  (c+160,"v linear_dut genblk1[1] w_r INDEX",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut genblk1[1] w_r DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+55,"v linear_dut genblk1[1] w_r addr",-1,31,0);
	vcdp->declBus  (c+56,"v linear_dut genblk1[1] w_r w_out",-1,31,0);
	vcdp->declArray(c+161,"v linear_dut genblk1[1] w_r FILENAME",-1,111,0);
	vcdp->declBus  (c+158,"v linear_dut genblk1[1] w_r SIZE",-1,31,0);
	vcdp->declBus  (c+165,"v linear_dut genblk1[1] w_r START_INDEX",-1,31,0);
	vcdp->declBus  (c+166,"v linear_dut genblk1[1] w_r END_INDEX",-1,31,0);
	// Tracing: v linear_dut genblk1[1] w_r rom // Ignored: Wide memory > --trace-max-array ents at ../rtl//linear.v:41
	vcdp->declBus  (c+148,"v linear_dut genblk1[2] w_r IN_CHANNELS",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk1[2] w_r OUT_CHANNELS",-1,31,0);
	vcdp->declQuad (c+151,"v linear_dut genblk1[2] w_r FN_PREFIX",-1,47,0);
	vcdp->declBus  (c+167,"v linear_dut genblk1[2] w_r INDEX",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut genblk1[2] w_r DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+57,"v linear_dut genblk1[2] w_r addr",-1,31,0);
	vcdp->declBus  (c+58,"v linear_dut genblk1[2] w_r w_out",-1,31,0);
	vcdp->declArray(c+168,"v linear_dut genblk1[2] w_r FILENAME",-1,111,0);
	vcdp->declBus  (c+158,"v linear_dut genblk1[2] w_r SIZE",-1,31,0);
	vcdp->declBus  (c+172,"v linear_dut genblk1[2] w_r START_INDEX",-1,31,0);
	vcdp->declBus  (c+173,"v linear_dut genblk1[2] w_r END_INDEX",-1,31,0);
	// Tracing: v linear_dut genblk1[2] w_r rom // Ignored: Wide memory > --trace-max-array ents at ../rtl//linear.v:41
	vcdp->declBus  (c+148,"v linear_dut genblk1[3] w_r IN_CHANNELS",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk1[3] w_r OUT_CHANNELS",-1,31,0);
	vcdp->declQuad (c+151,"v linear_dut genblk1[3] w_r FN_PREFIX",-1,47,0);
	vcdp->declBus  (c+174,"v linear_dut genblk1[3] w_r INDEX",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut genblk1[3] w_r DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+59,"v linear_dut genblk1[3] w_r addr",-1,31,0);
	vcdp->declBus  (c+60,"v linear_dut genblk1[3] w_r w_out",-1,31,0);
	vcdp->declArray(c+175,"v linear_dut genblk1[3] w_r FILENAME",-1,111,0);
	vcdp->declBus  (c+158,"v linear_dut genblk1[3] w_r SIZE",-1,31,0);
	vcdp->declBus  (c+179,"v linear_dut genblk1[3] w_r START_INDEX",-1,31,0);
	vcdp->declBus  (c+180,"v linear_dut genblk1[3] w_r END_INDEX",-1,31,0);
	// Tracing: v linear_dut genblk1[3] w_r rom // Ignored: Wide memory > --trace-max-array ents at ../rtl//linear.v:41
	vcdp->declBus  (c+148,"v linear_dut genblk1[4] w_r IN_CHANNELS",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk1[4] w_r OUT_CHANNELS",-1,31,0);
	vcdp->declQuad (c+151,"v linear_dut genblk1[4] w_r FN_PREFIX",-1,47,0);
	vcdp->declBus  (c+181,"v linear_dut genblk1[4] w_r INDEX",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut genblk1[4] w_r DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+61,"v linear_dut genblk1[4] w_r addr",-1,31,0);
	vcdp->declBus  (c+62,"v linear_dut genblk1[4] w_r w_out",-1,31,0);
	vcdp->declArray(c+182,"v linear_dut genblk1[4] w_r FILENAME",-1,111,0);
	vcdp->declBus  (c+158,"v linear_dut genblk1[4] w_r SIZE",-1,31,0);
	vcdp->declBus  (c+186,"v linear_dut genblk1[4] w_r START_INDEX",-1,31,0);
	vcdp->declBus  (c+187,"v linear_dut genblk1[4] w_r END_INDEX",-1,31,0);
	// Tracing: v linear_dut genblk1[4] w_r rom // Ignored: Wide memory > --trace-max-array ents at ../rtl//linear.v:41
	vcdp->declBus  (c+148,"v linear_dut genblk1[5] w_r IN_CHANNELS",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk1[5] w_r OUT_CHANNELS",-1,31,0);
	vcdp->declQuad (c+151,"v linear_dut genblk1[5] w_r FN_PREFIX",-1,47,0);
	vcdp->declBus  (c+188,"v linear_dut genblk1[5] w_r INDEX",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut genblk1[5] w_r DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+63,"v linear_dut genblk1[5] w_r addr",-1,31,0);
	vcdp->declBus  (c+64,"v linear_dut genblk1[5] w_r w_out",-1,31,0);
	vcdp->declArray(c+189,"v linear_dut genblk1[5] w_r FILENAME",-1,111,0);
	vcdp->declBus  (c+158,"v linear_dut genblk1[5] w_r SIZE",-1,31,0);
	vcdp->declBus  (c+193,"v linear_dut genblk1[5] w_r START_INDEX",-1,31,0);
	vcdp->declBus  (c+194,"v linear_dut genblk1[5] w_r END_INDEX",-1,31,0);
	// Tracing: v linear_dut genblk1[5] w_r rom // Ignored: Wide memory > --trace-max-array ents at ../rtl//linear.v:41
	vcdp->declBus  (c+148,"v linear_dut genblk1[6] w_r IN_CHANNELS",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk1[6] w_r OUT_CHANNELS",-1,31,0);
	vcdp->declQuad (c+151,"v linear_dut genblk1[6] w_r FN_PREFIX",-1,47,0);
	vcdp->declBus  (c+195,"v linear_dut genblk1[6] w_r INDEX",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut genblk1[6] w_r DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+65,"v linear_dut genblk1[6] w_r addr",-1,31,0);
	vcdp->declBus  (c+66,"v linear_dut genblk1[6] w_r w_out",-1,31,0);
	vcdp->declArray(c+196,"v linear_dut genblk1[6] w_r FILENAME",-1,111,0);
	vcdp->declBus  (c+158,"v linear_dut genblk1[6] w_r SIZE",-1,31,0);
	vcdp->declBus  (c+200,"v linear_dut genblk1[6] w_r START_INDEX",-1,31,0);
	vcdp->declBus  (c+201,"v linear_dut genblk1[6] w_r END_INDEX",-1,31,0);
	// Tracing: v linear_dut genblk1[6] w_r rom // Ignored: Wide memory > --trace-max-array ents at ../rtl//linear.v:41
	vcdp->declBus  (c+148,"v linear_dut genblk1[7] w_r IN_CHANNELS",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk1[7] w_r OUT_CHANNELS",-1,31,0);
	vcdp->declQuad (c+151,"v linear_dut genblk1[7] w_r FN_PREFIX",-1,47,0);
	vcdp->declBus  (c+202,"v linear_dut genblk1[7] w_r INDEX",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut genblk1[7] w_r DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+67,"v linear_dut genblk1[7] w_r addr",-1,31,0);
	vcdp->declBus  (c+68,"v linear_dut genblk1[7] w_r w_out",-1,31,0);
	vcdp->declArray(c+203,"v linear_dut genblk1[7] w_r FILENAME",-1,111,0);
	vcdp->declBus  (c+158,"v linear_dut genblk1[7] w_r SIZE",-1,31,0);
	vcdp->declBus  (c+207,"v linear_dut genblk1[7] w_r START_INDEX",-1,31,0);
	vcdp->declBus  (c+208,"v linear_dut genblk1[7] w_r END_INDEX",-1,31,0);
	// Tracing: v linear_dut genblk1[7] w_r rom // Ignored: Wide memory > --trace-max-array ents at ../rtl//linear.v:41
	vcdp->declBus  (c+148,"v linear_dut genblk1[8] w_r IN_CHANNELS",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk1[8] w_r OUT_CHANNELS",-1,31,0);
	vcdp->declQuad (c+151,"v linear_dut genblk1[8] w_r FN_PREFIX",-1,47,0);
	vcdp->declBus  (c+209,"v linear_dut genblk1[8] w_r INDEX",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut genblk1[8] w_r DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+69,"v linear_dut genblk1[8] w_r addr",-1,31,0);
	vcdp->declBus  (c+70,"v linear_dut genblk1[8] w_r w_out",-1,31,0);
	vcdp->declArray(c+210,"v linear_dut genblk1[8] w_r FILENAME",-1,111,0);
	vcdp->declBus  (c+158,"v linear_dut genblk1[8] w_r SIZE",-1,31,0);
	vcdp->declBus  (c+214,"v linear_dut genblk1[8] w_r START_INDEX",-1,31,0);
	vcdp->declBus  (c+215,"v linear_dut genblk1[8] w_r END_INDEX",-1,31,0);
	// Tracing: v linear_dut genblk1[8] w_r rom // Ignored: Wide memory > --trace-max-array ents at ../rtl//linear.v:41
	vcdp->declBus  (c+148,"v linear_dut genblk1[9] w_r IN_CHANNELS",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk1[9] w_r OUT_CHANNELS",-1,31,0);
	vcdp->declQuad (c+151,"v linear_dut genblk1[9] w_r FN_PREFIX",-1,47,0);
	vcdp->declBus  (c+216,"v linear_dut genblk1[9] w_r INDEX",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut genblk1[9] w_r DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+71,"v linear_dut genblk1[9] w_r addr",-1,31,0);
	vcdp->declBus  (c+72,"v linear_dut genblk1[9] w_r w_out",-1,31,0);
	vcdp->declArray(c+217,"v linear_dut genblk1[9] w_r FILENAME",-1,111,0);
	vcdp->declBus  (c+158,"v linear_dut genblk1[9] w_r SIZE",-1,31,0);
	vcdp->declBus  (c+221,"v linear_dut genblk1[9] w_r START_INDEX",-1,31,0);
	vcdp->declBus  (c+222,"v linear_dut genblk1[9] w_r END_INDEX",-1,31,0);
	// Tracing: v linear_dut genblk1[9] w_r rom // Ignored: Wide memory > --trace-max-array ents at ../rtl//linear.v:41
	vcdp->declBus  (c+150,"v linear_dut genblk2[0] mul DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk2[0] mul OUT_CHANNELS",-1,31,0);
	vcdp->declBus  (c+73,"v linear_dut genblk2[0] mul w_out",-1,31,0);
	vcdp->declBus  (c+74,"v linear_dut genblk2[0] mul d_in",-1,31,0);
	vcdp->declQuad (c+75,"v linear_dut genblk2[0] mul product",-1,63,0);
	vcdp->declBus  (c+150,"v linear_dut genblk2[1] mul DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk2[1] mul OUT_CHANNELS",-1,31,0);
	vcdp->declBus  (c+77,"v linear_dut genblk2[1] mul w_out",-1,31,0);
	vcdp->declBus  (c+78,"v linear_dut genblk2[1] mul d_in",-1,31,0);
	vcdp->declQuad (c+79,"v linear_dut genblk2[1] mul product",-1,63,0);
	vcdp->declBus  (c+150,"v linear_dut genblk2[2] mul DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk2[2] mul OUT_CHANNELS",-1,31,0);
	vcdp->declBus  (c+81,"v linear_dut genblk2[2] mul w_out",-1,31,0);
	vcdp->declBus  (c+82,"v linear_dut genblk2[2] mul d_in",-1,31,0);
	vcdp->declQuad (c+83,"v linear_dut genblk2[2] mul product",-1,63,0);
	vcdp->declBus  (c+150,"v linear_dut genblk2[3] mul DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk2[3] mul OUT_CHANNELS",-1,31,0);
	vcdp->declBus  (c+85,"v linear_dut genblk2[3] mul w_out",-1,31,0);
	vcdp->declBus  (c+86,"v linear_dut genblk2[3] mul d_in",-1,31,0);
	vcdp->declQuad (c+87,"v linear_dut genblk2[3] mul product",-1,63,0);
	vcdp->declBus  (c+150,"v linear_dut genblk2[4] mul DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk2[4] mul OUT_CHANNELS",-1,31,0);
	vcdp->declBus  (c+89,"v linear_dut genblk2[4] mul w_out",-1,31,0);
	vcdp->declBus  (c+90,"v linear_dut genblk2[4] mul d_in",-1,31,0);
	vcdp->declQuad (c+91,"v linear_dut genblk2[4] mul product",-1,63,0);
	vcdp->declBus  (c+150,"v linear_dut genblk2[5] mul DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk2[5] mul OUT_CHANNELS",-1,31,0);
	vcdp->declBus  (c+93,"v linear_dut genblk2[5] mul w_out",-1,31,0);
	vcdp->declBus  (c+94,"v linear_dut genblk2[5] mul d_in",-1,31,0);
	vcdp->declQuad (c+95,"v linear_dut genblk2[5] mul product",-1,63,0);
	vcdp->declBus  (c+150,"v linear_dut genblk2[6] mul DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk2[6] mul OUT_CHANNELS",-1,31,0);
	vcdp->declBus  (c+97,"v linear_dut genblk2[6] mul w_out",-1,31,0);
	vcdp->declBus  (c+98,"v linear_dut genblk2[6] mul d_in",-1,31,0);
	vcdp->declQuad (c+99,"v linear_dut genblk2[6] mul product",-1,63,0);
	vcdp->declBus  (c+150,"v linear_dut genblk2[7] mul DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk2[7] mul OUT_CHANNELS",-1,31,0);
	vcdp->declBus  (c+101,"v linear_dut genblk2[7] mul w_out",-1,31,0);
	vcdp->declBus  (c+102,"v linear_dut genblk2[7] mul d_in",-1,31,0);
	vcdp->declQuad (c+103,"v linear_dut genblk2[7] mul product",-1,63,0);
	vcdp->declBus  (c+150,"v linear_dut genblk2[8] mul DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk2[8] mul OUT_CHANNELS",-1,31,0);
	vcdp->declBus  (c+105,"v linear_dut genblk2[8] mul w_out",-1,31,0);
	vcdp->declBus  (c+106,"v linear_dut genblk2[8] mul d_in",-1,31,0);
	vcdp->declQuad (c+107,"v linear_dut genblk2[8] mul product",-1,63,0);
	vcdp->declBus  (c+150,"v linear_dut genblk2[9] mul DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+149,"v linear_dut genblk2[9] mul OUT_CHANNELS",-1,31,0);
	vcdp->declBus  (c+109,"v linear_dut genblk2[9] mul w_out",-1,31,0);
	vcdp->declBus  (c+110,"v linear_dut genblk2[9] mul d_in",-1,31,0);
	vcdp->declQuad (c+111,"v linear_dut genblk2[9] mul product",-1,63,0);
	vcdp->declBus  (c+150,"v linear_dut genblk3[0] acc DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+113,"v linear_dut genblk3[0] acc d_in",-1,31,0);
	vcdp->declBus  (c+114,"v linear_dut genblk3[0] acc d_out",-1,31,0);
	vcdp->declBit  (c+143,"v linear_dut genblk3[0] acc clk",-1);
	vcdp->declBit  (c+52,"v linear_dut genblk3[0] acc en",-1);
	vcdp->declBit  (c+53,"v linear_dut genblk3[0] acc rst",-1);
	vcdp->declBus  (c+114,"v linear_dut genblk3[0] acc d_acc",-1,31,0);
	vcdp->declBus  (c+223,"v linear_dut genblk3[0] acc i",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut genblk3[1] acc DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+115,"v linear_dut genblk3[1] acc d_in",-1,31,0);
	vcdp->declBus  (c+116,"v linear_dut genblk3[1] acc d_out",-1,31,0);
	vcdp->declBit  (c+143,"v linear_dut genblk3[1] acc clk",-1);
	vcdp->declBit  (c+52,"v linear_dut genblk3[1] acc en",-1);
	vcdp->declBit  (c+53,"v linear_dut genblk3[1] acc rst",-1);
	vcdp->declBus  (c+116,"v linear_dut genblk3[1] acc d_acc",-1,31,0);
	vcdp->declBus  (c+223,"v linear_dut genblk3[1] acc i",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut genblk3[2] acc DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+117,"v linear_dut genblk3[2] acc d_in",-1,31,0);
	vcdp->declBus  (c+118,"v linear_dut genblk3[2] acc d_out",-1,31,0);
	vcdp->declBit  (c+143,"v linear_dut genblk3[2] acc clk",-1);
	vcdp->declBit  (c+52,"v linear_dut genblk3[2] acc en",-1);
	vcdp->declBit  (c+53,"v linear_dut genblk3[2] acc rst",-1);
	vcdp->declBus  (c+118,"v linear_dut genblk3[2] acc d_acc",-1,31,0);
	vcdp->declBus  (c+223,"v linear_dut genblk3[2] acc i",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut genblk3[3] acc DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+119,"v linear_dut genblk3[3] acc d_in",-1,31,0);
	vcdp->declBus  (c+120,"v linear_dut genblk3[3] acc d_out",-1,31,0);
	vcdp->declBit  (c+143,"v linear_dut genblk3[3] acc clk",-1);
	vcdp->declBit  (c+52,"v linear_dut genblk3[3] acc en",-1);
	vcdp->declBit  (c+53,"v linear_dut genblk3[3] acc rst",-1);
	vcdp->declBus  (c+120,"v linear_dut genblk3[3] acc d_acc",-1,31,0);
	vcdp->declBus  (c+223,"v linear_dut genblk3[3] acc i",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut genblk3[4] acc DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+121,"v linear_dut genblk3[4] acc d_in",-1,31,0);
	vcdp->declBus  (c+122,"v linear_dut genblk3[4] acc d_out",-1,31,0);
	vcdp->declBit  (c+143,"v linear_dut genblk3[4] acc clk",-1);
	vcdp->declBit  (c+52,"v linear_dut genblk3[4] acc en",-1);
	vcdp->declBit  (c+53,"v linear_dut genblk3[4] acc rst",-1);
	vcdp->declBus  (c+122,"v linear_dut genblk3[4] acc d_acc",-1,31,0);
	vcdp->declBus  (c+223,"v linear_dut genblk3[4] acc i",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut genblk3[5] acc DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+123,"v linear_dut genblk3[5] acc d_in",-1,31,0);
	vcdp->declBus  (c+124,"v linear_dut genblk3[5] acc d_out",-1,31,0);
	vcdp->declBit  (c+143,"v linear_dut genblk3[5] acc clk",-1);
	vcdp->declBit  (c+52,"v linear_dut genblk3[5] acc en",-1);
	vcdp->declBit  (c+53,"v linear_dut genblk3[5] acc rst",-1);
	vcdp->declBus  (c+124,"v linear_dut genblk3[5] acc d_acc",-1,31,0);
	vcdp->declBus  (c+223,"v linear_dut genblk3[5] acc i",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut genblk3[6] acc DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+125,"v linear_dut genblk3[6] acc d_in",-1,31,0);
	vcdp->declBus  (c+126,"v linear_dut genblk3[6] acc d_out",-1,31,0);
	vcdp->declBit  (c+143,"v linear_dut genblk3[6] acc clk",-1);
	vcdp->declBit  (c+52,"v linear_dut genblk3[6] acc en",-1);
	vcdp->declBit  (c+53,"v linear_dut genblk3[6] acc rst",-1);
	vcdp->declBus  (c+126,"v linear_dut genblk3[6] acc d_acc",-1,31,0);
	vcdp->declBus  (c+223,"v linear_dut genblk3[6] acc i",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut genblk3[7] acc DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+127,"v linear_dut genblk3[7] acc d_in",-1,31,0);
	vcdp->declBus  (c+128,"v linear_dut genblk3[7] acc d_out",-1,31,0);
	vcdp->declBit  (c+143,"v linear_dut genblk3[7] acc clk",-1);
	vcdp->declBit  (c+52,"v linear_dut genblk3[7] acc en",-1);
	vcdp->declBit  (c+53,"v linear_dut genblk3[7] acc rst",-1);
	vcdp->declBus  (c+128,"v linear_dut genblk3[7] acc d_acc",-1,31,0);
	vcdp->declBus  (c+223,"v linear_dut genblk3[7] acc i",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut genblk3[8] acc DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+129,"v linear_dut genblk3[8] acc d_in",-1,31,0);
	vcdp->declBus  (c+130,"v linear_dut genblk3[8] acc d_out",-1,31,0);
	vcdp->declBit  (c+143,"v linear_dut genblk3[8] acc clk",-1);
	vcdp->declBit  (c+52,"v linear_dut genblk3[8] acc en",-1);
	vcdp->declBit  (c+53,"v linear_dut genblk3[8] acc rst",-1);
	vcdp->declBus  (c+130,"v linear_dut genblk3[8] acc d_acc",-1,31,0);
	vcdp->declBus  (c+223,"v linear_dut genblk3[8] acc i",-1,31,0);
	vcdp->declBus  (c+150,"v linear_dut genblk3[9] acc DATA_WIDTH",-1,31,0);
	vcdp->declBus  (c+131,"v linear_dut genblk3[9] acc d_in",-1,31,0);
	vcdp->declBus  (c+132,"v linear_dut genblk3[9] acc d_out",-1,31,0);
	vcdp->declBit  (c+143,"v linear_dut genblk3[9] acc clk",-1);
	vcdp->declBit  (c+52,"v linear_dut genblk3[9] acc en",-1);
	vcdp->declBit  (c+53,"v linear_dut genblk3[9] acc rst",-1);
	vcdp->declBus  (c+132,"v linear_dut genblk3[9] acc d_acc",-1,31,0);
	vcdp->declBus  (c+223,"v linear_dut genblk3[9] acc i",-1,31,0);
    }
}

void Vlinear::traceFullThis__1(Vlinear__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code) {
    Vlinear* __restrict vlTOPp VL_ATTR_UNUSED = vlSymsp->TOPp;
    int c=code;
    if (0 && vcdp && c) {}  // Prevent unused
    // Variables
    VL_SIGW(__Vtemp22,127,0,4);
    VL_SIGW(__Vtemp23,127,0,4);
    VL_SIGW(__Vtemp24,127,0,4);
    VL_SIGW(__Vtemp25,127,0,4);
    VL_SIGW(__Vtemp26,127,0,4);
    VL_SIGW(__Vtemp27,127,0,4);
    VL_SIGW(__Vtemp28,127,0,4);
    VL_SIGW(__Vtemp29,127,0,4);
    VL_SIGW(__Vtemp30,127,0,4);
    VL_SIGW(__Vtemp31,127,0,4);
    // Body
    {
	vcdp->fullBus  (c+1,(vlTOPp->v__DOT__d_outs[0]),32);
	vcdp->fullBus  (c+2,(vlTOPp->v__DOT__d_outs[1]),32);
	vcdp->fullBus  (c+3,(vlTOPp->v__DOT__d_outs[2]),32);
	vcdp->fullBus  (c+4,(vlTOPp->v__DOT__d_outs[3]),32);
	vcdp->fullBus  (c+5,(vlTOPp->v__DOT__d_outs[4]),32);
	vcdp->fullBus  (c+6,(vlTOPp->v__DOT__d_outs[5]),32);
	vcdp->fullBus  (c+7,(vlTOPp->v__DOT__d_outs[6]),32);
	vcdp->fullBus  (c+8,(vlTOPp->v__DOT__d_outs[7]),32);
	vcdp->fullBus  (c+9,(vlTOPp->v__DOT__d_outs[8]),32);
	vcdp->fullBus  (c+10,(vlTOPp->v__DOT__d_outs[9]),32);
	vcdp->fullBus  (c+11,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[0]),32);
	vcdp->fullBus  (c+12,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[1]),32);
	vcdp->fullBus  (c+13,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[2]),32);
	vcdp->fullBus  (c+14,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[3]),32);
	vcdp->fullBus  (c+15,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[4]),32);
	vcdp->fullBus  (c+16,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[5]),32);
	vcdp->fullBus  (c+17,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[6]),32);
	vcdp->fullBus  (c+18,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[7]),32);
	vcdp->fullBus  (c+19,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[8]),32);
	vcdp->fullBus  (c+20,(vlTOPp->v__DOT__linear_dut__DOT__d_outs[9]),32);
	vcdp->fullBus  (c+21,(vlTOPp->v__DOT__linear_dut__DOT__addr),32);
	vcdp->fullBus  (c+22,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[0]),32);
	vcdp->fullBus  (c+23,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[1]),32);
	vcdp->fullBus  (c+24,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[2]),32);
	vcdp->fullBus  (c+25,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[3]),32);
	vcdp->fullBus  (c+26,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[4]),32);
	vcdp->fullBus  (c+27,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[5]),32);
	vcdp->fullBus  (c+28,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[6]),32);
	vcdp->fullBus  (c+29,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[7]),32);
	vcdp->fullBus  (c+30,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[8]),32);
	vcdp->fullBus  (c+31,(vlTOPp->v__DOT__linear_dut__DOT__w_outs[9]),32);
	vcdp->fullQuad (c+32,(vlTOPp->v__DOT__linear_dut__DOT__products[0]),64);
	vcdp->fullQuad (c+34,(vlTOPp->v__DOT__linear_dut__DOT__products[1]),64);
	vcdp->fullQuad (c+36,(vlTOPp->v__DOT__linear_dut__DOT__products[2]),64);
	vcdp->fullQuad (c+38,(vlTOPp->v__DOT__linear_dut__DOT__products[3]),64);
	vcdp->fullQuad (c+40,(vlTOPp->v__DOT__linear_dut__DOT__products[4]),64);
	vcdp->fullQuad (c+42,(vlTOPp->v__DOT__linear_dut__DOT__products[5]),64);
	vcdp->fullQuad (c+44,(vlTOPp->v__DOT__linear_dut__DOT__products[6]),64);
	vcdp->fullQuad (c+46,(vlTOPp->v__DOT__linear_dut__DOT__products[7]),64);
	vcdp->fullQuad (c+48,(vlTOPp->v__DOT__linear_dut__DOT__products[8]),64);
	vcdp->fullQuad (c+50,(vlTOPp->v__DOT__linear_dut__DOT__products[9]),64);
	vcdp->fullBus  (c+54,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__0__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->fullBus  (c+55,(((IData)(0x41U) + vlTOPp->v__DOT__linear_dut__DOT__addr)),32);
	vcdp->fullBus  (c+56,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__1__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->fullBus  (c+57,(((IData)(0x82U) + vlTOPp->v__DOT__linear_dut__DOT__addr)),32);
	vcdp->fullBus  (c+58,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__2__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->fullBus  (c+59,(((IData)(0xc3U) + vlTOPp->v__DOT__linear_dut__DOT__addr)),32);
	vcdp->fullBus  (c+60,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__3__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->fullBus  (c+61,(((IData)(0x104U) + vlTOPp->v__DOT__linear_dut__DOT__addr)),32);
	vcdp->fullBus  (c+62,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__4__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->fullBus  (c+63,(((IData)(0x145U) + vlTOPp->v__DOT__linear_dut__DOT__addr)),32);
	vcdp->fullBus  (c+64,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__5__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->fullBus  (c+65,(((IData)(0x186U) + vlTOPp->v__DOT__linear_dut__DOT__addr)),32);
	vcdp->fullBus  (c+66,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__6__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->fullBus  (c+67,(((IData)(0x1c7U) + vlTOPp->v__DOT__linear_dut__DOT__addr)),32);
	vcdp->fullBus  (c+68,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__7__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->fullBus  (c+69,(((IData)(0x208U) + vlTOPp->v__DOT__linear_dut__DOT__addr)),32);
	vcdp->fullBus  (c+70,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__8__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->fullBus  (c+71,(((IData)(0x249U) + vlTOPp->v__DOT__linear_dut__DOT__addr)),32);
	vcdp->fullBus  (c+72,(vlTOPp->v__DOT__linear_dut__DOT__genblk1__BRA__9__KET____DOT____Vcellout__w_r__w_out),32);
	vcdp->fullBus  (c+73,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->fullBus  (c+74,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->fullQuad (c+75,(VL_MULS_QQQ(64,64,64, 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__w_out), 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->fullBus  (c+77,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->fullBus  (c+78,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->fullQuad (c+79,(VL_MULS_QQQ(64,64,64, 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__w_out), 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->fullBus  (c+81,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->fullBus  (c+82,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->fullQuad (c+83,(VL_MULS_QQQ(64,64,64, 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__w_out), 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->fullBus  (c+85,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->fullBus  (c+86,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->fullQuad (c+87,(VL_MULS_QQQ(64,64,64, 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__w_out), 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->fullBus  (c+89,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->fullBus  (c+90,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->fullQuad (c+91,(VL_MULS_QQQ(64,64,64, 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__w_out), 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->fullBus  (c+93,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->fullBus  (c+94,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->fullQuad (c+95,(VL_MULS_QQQ(64,64,64, 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__w_out), 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->fullBus  (c+97,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->fullBus  (c+98,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->fullQuad (c+99,(VL_MULS_QQQ(64,64,64, 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__w_out), 
					  VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->fullBus  (c+101,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->fullBus  (c+102,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->fullQuad (c+103,(VL_MULS_QQQ(64,64,64, 
					   VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__w_out), 
					   VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->fullBus  (c+105,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->fullBus  (c+106,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->fullQuad (c+107,(VL_MULS_QQQ(64,64,64, 
					   VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__w_out), 
					   VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->fullBus  (c+109,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__w_out),32);
	vcdp->fullBus  (c+110,(vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__d_in),32);
	vcdp->fullQuad (c+111,(VL_MULS_QQQ(64,64,64, 
					   VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__w_out), 
					   VL_EXTENDS_QI(64,32, vlTOPp->v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__d_in))),64);
	vcdp->fullBus  (c+113,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__0__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->fullBus  (c+114,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__0__KET____DOT__acc__DOT__d_acc),32);
	vcdp->fullBus  (c+115,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__1__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->fullBus  (c+116,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__1__KET____DOT__acc__DOT__d_acc),32);
	vcdp->fullBus  (c+117,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__2__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->fullBus  (c+118,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__2__KET____DOT__acc__DOT__d_acc),32);
	vcdp->fullBus  (c+119,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__3__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->fullBus  (c+120,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__3__KET____DOT__acc__DOT__d_acc),32);
	vcdp->fullBus  (c+121,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__4__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->fullBus  (c+122,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__4__KET____DOT__acc__DOT__d_acc),32);
	vcdp->fullBus  (c+123,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__5__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->fullBus  (c+124,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__5__KET____DOT__acc__DOT__d_acc),32);
	vcdp->fullBus  (c+125,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__6__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->fullBus  (c+126,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__6__KET____DOT__acc__DOT__d_acc),32);
	vcdp->fullBus  (c+127,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__7__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->fullBus  (c+128,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__7__KET____DOT__acc__DOT__d_acc),32);
	vcdp->fullBus  (c+129,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__8__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->fullBus  (c+130,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__8__KET____DOT__acc__DOT__d_acc),32);
	vcdp->fullBus  (c+131,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__9__KET____DOT____Vcellinp__acc__d_in),32);
	vcdp->fullBus  (c+132,(vlTOPp->v__DOT__linear_dut__DOT__genblk3__BRA__9__KET____DOT__acc__DOT__d_acc),32);
	vcdp->fullBit  (c+52,(vlTOPp->v__DOT__linear_dut__DOT__acc_en));
	vcdp->fullBit  (c+53,(vlTOPp->v__DOT__linear_dut__DOT__acc_rst));
	vcdp->fullBus  (c+133,(vlTOPp->d_outs[0]),32);
	vcdp->fullBus  (c+134,(vlTOPp->d_outs[1]),32);
	vcdp->fullBus  (c+135,(vlTOPp->d_outs[2]),32);
	vcdp->fullBus  (c+136,(vlTOPp->d_outs[3]),32);
	vcdp->fullBus  (c+137,(vlTOPp->d_outs[4]),32);
	vcdp->fullBus  (c+138,(vlTOPp->d_outs[5]),32);
	vcdp->fullBus  (c+139,(vlTOPp->d_outs[6]),32);
	vcdp->fullBus  (c+140,(vlTOPp->d_outs[7]),32);
	vcdp->fullBus  (c+141,(vlTOPp->d_outs[8]),32);
	vcdp->fullBus  (c+142,(vlTOPp->d_outs[9]),32);
	vcdp->fullBit  (c+144,(vlTOPp->start));
	vcdp->fullBit  (c+145,(vlTOPp->rst));
	vcdp->fullBit  (c+146,(vlTOPp->busy));
	vcdp->fullBit  (c+147,(vlTOPp->done));
	vcdp->fullBit  (c+143,(vlTOPp->clk));
	vcdp->fullBus  (c+153,(0U),32);
	__Vtemp22[0U] = 0x2e646174U;
	__Vtemp22[1U] = 0x2d303030U;
	__Vtemp22[2U] = 0x79657234U;
	__Vtemp22[3U] = 0x6c61U;
	vcdp->fullArray(c+154,(__Vtemp22),112);
	vcdp->fullBus  (c+159,(0x40U),32);
	vcdp->fullBus  (c+160,(1U),32);
	__Vtemp23[0U] = 0x2e646174U;
	__Vtemp23[1U] = 0x2d303031U;
	__Vtemp23[2U] = 0x79657234U;
	__Vtemp23[3U] = 0x6c61U;
	vcdp->fullArray(c+161,(__Vtemp23),112);
	vcdp->fullBus  (c+165,(0x41U),32);
	vcdp->fullBus  (c+166,(0x81U),32);
	vcdp->fullBus  (c+167,(2U),32);
	__Vtemp24[0U] = 0x2e646174U;
	__Vtemp24[1U] = 0x2d303032U;
	__Vtemp24[2U] = 0x79657234U;
	__Vtemp24[3U] = 0x6c61U;
	vcdp->fullArray(c+168,(__Vtemp24),112);
	vcdp->fullBus  (c+172,(0x82U),32);
	vcdp->fullBus  (c+173,(0xc2U),32);
	vcdp->fullBus  (c+174,(3U),32);
	__Vtemp25[0U] = 0x2e646174U;
	__Vtemp25[1U] = 0x2d303033U;
	__Vtemp25[2U] = 0x79657234U;
	__Vtemp25[3U] = 0x6c61U;
	vcdp->fullArray(c+175,(__Vtemp25),112);
	vcdp->fullBus  (c+179,(0xc3U),32);
	vcdp->fullBus  (c+180,(0x103U),32);
	vcdp->fullBus  (c+181,(4U),32);
	__Vtemp26[0U] = 0x2e646174U;
	__Vtemp26[1U] = 0x2d303034U;
	__Vtemp26[2U] = 0x79657234U;
	__Vtemp26[3U] = 0x6c61U;
	vcdp->fullArray(c+182,(__Vtemp26),112);
	vcdp->fullBus  (c+186,(0x104U),32);
	vcdp->fullBus  (c+187,(0x144U),32);
	vcdp->fullBus  (c+188,(5U),32);
	__Vtemp27[0U] = 0x2e646174U;
	__Vtemp27[1U] = 0x2d303035U;
	__Vtemp27[2U] = 0x79657234U;
	__Vtemp27[3U] = 0x6c61U;
	vcdp->fullArray(c+189,(__Vtemp27),112);
	vcdp->fullBus  (c+193,(0x145U),32);
	vcdp->fullBus  (c+194,(0x185U),32);
	vcdp->fullBus  (c+195,(6U),32);
	__Vtemp28[0U] = 0x2e646174U;
	__Vtemp28[1U] = 0x2d303036U;
	__Vtemp28[2U] = 0x79657234U;
	__Vtemp28[3U] = 0x6c61U;
	vcdp->fullArray(c+196,(__Vtemp28),112);
	vcdp->fullBus  (c+200,(0x186U),32);
	vcdp->fullBus  (c+201,(0x1c6U),32);
	vcdp->fullBus  (c+202,(7U),32);
	__Vtemp29[0U] = 0x2e646174U;
	__Vtemp29[1U] = 0x2d303037U;
	__Vtemp29[2U] = 0x79657234U;
	__Vtemp29[3U] = 0x6c61U;
	vcdp->fullArray(c+203,(__Vtemp29),112);
	vcdp->fullBus  (c+207,(0x1c7U),32);
	vcdp->fullBus  (c+208,(0x207U),32);
	vcdp->fullBus  (c+209,(8U),32);
	__Vtemp30[0U] = 0x2e646174U;
	__Vtemp30[1U] = 0x2d303038U;
	__Vtemp30[2U] = 0x79657234U;
	__Vtemp30[3U] = 0x6c61U;
	vcdp->fullArray(c+210,(__Vtemp30),112);
	vcdp->fullBus  (c+214,(0x208U),32);
	vcdp->fullBus  (c+215,(0x248U),32);
	vcdp->fullBus  (c+148,(0x40U),32);
	vcdp->fullQuad (c+151,(VL_ULL(0x6c6179657234)),48);
	vcdp->fullBus  (c+216,(9U),32);
	__Vtemp31[0U] = 0x2e646174U;
	__Vtemp31[1U] = 0x2d303039U;
	__Vtemp31[2U] = 0x79657234U;
	__Vtemp31[3U] = 0x6c61U;
	vcdp->fullArray(c+217,(__Vtemp31),112);
	vcdp->fullBus  (c+158,(0x28aU),32);
	vcdp->fullBus  (c+221,(0x249U),32);
	vcdp->fullBus  (c+222,(0x289U),32);
	vcdp->fullBus  (c+149,(0xaU),32);
	vcdp->fullBus  (c+150,(0x20U),32);
	vcdp->fullBus  (c+223,(0U),32);
    }
}
