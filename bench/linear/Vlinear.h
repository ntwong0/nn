// Verilated -*- C++ -*-
// DESCRIPTION: Verilator output: Primary design header
//
// This header should be included by all source files instantiating the design.
// The class here is then constructed to instantiate the design.
// See the Verilator manual for examples.

#ifndef _Vlinear_H_
#define _Vlinear_H_

#include "verilated.h"
class Vlinear__Syms;
class VerilatedVcd;

//----------

VL_MODULE(Vlinear) {
  public:
    // CELLS
    // Public to allow access to /*verilator_public*/ items;
    // otherwise the application code can consider these internals.
    
    // PORTS
    // The application code writes and reads these signals to
    // propagate new values into/out from the Verilated model.
    VL_IN8(clk,0,0);
    VL_IN8(start,0,0);
    VL_IN8(rst,0,0);
    VL_OUT8(busy,0,0);
    VL_OUT8(done,0,0);
    //char	__VpadToAlign5[3];
    VL_OUT(d_outs[10],31,0);
    
    // LOCAL SIGNALS
    // Internals; generally not touched by application code
    VL_SIG8(v__DOT__linear_dut__DOT__acc_en,0,0);
    VL_SIG8(v__DOT__linear_dut__DOT__acc_rst,0,0);
    //char	__VpadToAlign54[2];
    VL_SIG(v__DOT__linear_dut__DOT__addr,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__0__KET____DOT__acc__DOT__d_acc,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__1__KET____DOT__acc__DOT__d_acc,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__2__KET____DOT__acc__DOT__d_acc,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__3__KET____DOT__acc__DOT__d_acc,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__4__KET____DOT__acc__DOT__d_acc,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__5__KET____DOT__acc__DOT__d_acc,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__6__KET____DOT__acc__DOT__d_acc,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__7__KET____DOT__acc__DOT__d_acc,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__8__KET____DOT__acc__DOT__d_acc,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__9__KET____DOT__acc__DOT__d_acc,31,0);
    //char	__VpadToAlign100[4];
    VL_SIG(v__DOT__d_outs[10],31,0);
    VL_SIG(v__DOT__d_outs_prev[65],31,0);
    //char	__VpadToAlign404[4];
    VL_SIG(v__DOT__linear_dut__DOT__d_outs[10],31,0);
    VL_SIG(v__DOT__linear_dut__DOT__w_outs[10],31,0);
    VL_SIG64(v__DOT__linear_dut__DOT__products[10],63,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__0__KET____DOT__w_r__DOT__rom[65],31,0);
    //char	__VpadToAlign828[4];
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__1__KET____DOT__w_r__DOT__rom[65],31,0);
    //char	__VpadToAlign1092[4];
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__2__KET____DOT__w_r__DOT__rom[65],31,0);
    //char	__VpadToAlign1356[4];
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__3__KET____DOT__w_r__DOT__rom[65],31,0);
    //char	__VpadToAlign1620[4];
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__4__KET____DOT__w_r__DOT__rom[65],31,0);
    //char	__VpadToAlign1884[4];
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__5__KET____DOT__w_r__DOT__rom[65],31,0);
    //char	__VpadToAlign2148[4];
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__6__KET____DOT__w_r__DOT__rom[65],31,0);
    //char	__VpadToAlign2412[4];
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__7__KET____DOT__w_r__DOT__rom[65],31,0);
    //char	__VpadToAlign2676[4];
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__8__KET____DOT__w_r__DOT__rom[65],31,0);
    //char	__VpadToAlign2940[4];
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__9__KET____DOT__w_r__DOT__rom[65],31,0);
    
    // LOCAL VARIABLES
    // Internals; generally not touched by application code
    VL_SIG8(__Vclklast__TOP__clk,0,0);
    //char	__VpadToAlign3209[3];
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__0__KET____DOT____Vcellout__w_r__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__1__KET____DOT____Vcellout__w_r__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__2__KET____DOT____Vcellout__w_r__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__3__KET____DOT____Vcellout__w_r__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__4__KET____DOT____Vcellout__w_r__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__5__KET____DOT____Vcellout__w_r__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__6__KET____DOT____Vcellout__w_r__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__7__KET____DOT____Vcellout__w_r__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__8__KET____DOT____Vcellout__w_r__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk1__BRA__9__KET____DOT____Vcellout__w_r__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__d_in,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__0__KET____DOT____Vcellinp__mul__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__d_in,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__1__KET____DOT____Vcellinp__mul__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__d_in,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__2__KET____DOT____Vcellinp__mul__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__d_in,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__3__KET____DOT____Vcellinp__mul__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__d_in,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__4__KET____DOT____Vcellinp__mul__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__d_in,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__5__KET____DOT____Vcellinp__mul__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__d_in,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__6__KET____DOT____Vcellinp__mul__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__d_in,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__7__KET____DOT____Vcellinp__mul__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__d_in,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__8__KET____DOT____Vcellinp__mul__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__d_in,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk2__BRA__9__KET____DOT____Vcellinp__mul__w_out,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__0__KET____DOT____Vcellinp__acc__d_in,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__1__KET____DOT____Vcellinp__acc__d_in,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__2__KET____DOT____Vcellinp__acc__d_in,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__3__KET____DOT____Vcellinp__acc__d_in,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__4__KET____DOT____Vcellinp__acc__d_in,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__5__KET____DOT____Vcellinp__acc__d_in,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__6__KET____DOT____Vcellinp__acc__d_in,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__7__KET____DOT____Vcellinp__acc__d_in,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__8__KET____DOT____Vcellinp__acc__d_in,31,0);
    VL_SIG(v__DOT__linear_dut__DOT__genblk3__BRA__9__KET____DOT____Vcellinp__acc__d_in,31,0);
    VL_SIG(__Vm_traceActivity,31,0);
    
    // INTERNAL VARIABLES
    // Internals; generally not touched by application code
    //char	__VpadToAlign3380[4];
    Vlinear__Syms*	__VlSymsp;		// Symbol table
    
    // PARAMETERS
    // Parameters marked /*verilator public*/ for use by application code
    
    // CONSTRUCTORS
  private:
    Vlinear& operator= (const Vlinear&);	///< Copying not allowed
    Vlinear(const Vlinear&);	///< Copying not allowed
  public:
    /// Construct the model; called by application code
    /// The special name  may be used to make a wrapper with a
    /// single model invisible WRT DPI scope names.
    Vlinear(const char* name="TOP");
    /// Destroy the model; called (often implicitly) by application code
    ~Vlinear();
    /// Trace signals in the model; called by application code
    void trace (VerilatedVcdC* tfp, int levels, int options=0);
    
    // USER METHODS
    
    // API METHODS
    /// Evaluate the model.  Application must call when inputs change.
    void eval();
    /// Simulation complete, run final blocks.  Application must call on completion.
    void final();
    
    // INTERNAL METHODS
  private:
    static void _eval_initial_loop(Vlinear__Syms* __restrict vlSymsp);
  public:
    void __Vconfigure(Vlinear__Syms* symsp, bool first);
  private:
    static QData	_change_request(Vlinear__Syms* __restrict vlSymsp);
  public:
    static void	_eval(Vlinear__Syms* __restrict vlSymsp);
    static void	_eval_initial(Vlinear__Syms* __restrict vlSymsp);
    static void	_eval_settle(Vlinear__Syms* __restrict vlSymsp);
    static void	_initial__TOP__1(Vlinear__Syms* __restrict vlSymsp);
    static void	_initial__TOP__3(Vlinear__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__2(Vlinear__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__4(Vlinear__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__6(Vlinear__Syms* __restrict vlSymsp);
    static void	_sequent__TOP__8(Vlinear__Syms* __restrict vlSymsp);
    static void	_settle__TOP__5(Vlinear__Syms* __restrict vlSymsp);
    static void	_settle__TOP__7(Vlinear__Syms* __restrict vlSymsp);
    static void	_settle__TOP__9(Vlinear__Syms* __restrict vlSymsp);
    static void	traceChgThis(Vlinear__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code);
    static void	traceChgThis__2(Vlinear__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code);
    static void	traceChgThis__3(Vlinear__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code);
    static void	traceFullThis(Vlinear__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code);
    static void	traceFullThis__1(Vlinear__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code);
    static void	traceInitThis(Vlinear__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code);
    static void	traceInitThis__1(Vlinear__Syms* __restrict vlSymsp, VerilatedVcd* vcdp, uint32_t code);
    static void traceInit (VerilatedVcd* vcdp, void* userthis, uint32_t code);
    static void traceFull (VerilatedVcd* vcdp, void* userthis, uint32_t code);
    static void traceChg  (VerilatedVcd* vcdp, void* userthis, uint32_t code);
} VL_ATTR_ALIGNED(128);

#endif  /*guard*/
