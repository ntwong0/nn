#include <verilated.h>
#include "Vpulsedemo_top.h"

#include "verilated_vcd_c.h"

#include <iostream>
#include <string>
#include <cstdlib>
#include <cstdio>

Vpulsedemo_top *uut;
vluint64_t main_time = 0; // ns

double sc_time_stamp () {
	return main_time;
}

int main(int argc, char** argv)
{
	bool vcdTrace = true;
	VerilatedVcdC* tfp = NULL;

	Verilated::commandArgs(argc, argv);
	uut = new Vpulsedemo_top;

	uut->eval();
	uut->eval();

	if (vcdTrace)
	{
		Verilated::traceEverOn(true);

		tfp = new VerilatedVcdC;
		uut->trace(tfp, 99);

		std::string vcdname = argv[0];
		vcdname += ".vcd";
		std::cout << vcdname << std::endl;
		tfp->open(vcdname.c_str());
	}

	uut->CLOCK = 0;
	uut->eval();

    bool overtime = false;
	while (!Verilated::gotFinish() && !overtime)
	{
		uut->CLOCK = uut->CLOCK ? 0 : 1;
		uut->eval();

		if (tfp != NULL)
		{
			tfp->dump (main_time);
		}

        if (main_time % 1000 == 0)
        {
            printf("main_time: %i\n", main_time);
        }

		main_time++;
        overtime = (main_time > 30000) ? true : false;
	}

	uut->final();

	if (tfp != NULL)
	{
		tfp->close();
		delete tfp;
	}

	delete uut;

	return 0;
}

